"""
Pressure Pulse
"""
import numpy as np
import os

from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application

# local files
from edac import EDACScheme
from inlet_outlet_manager import (
        InletInfo, OutletInfo)

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W):
        self.U = U
        self.V = V
        self.W = W

        super(ResetInletVelocity, self).__init__(dest, sources)

    def initialize(self, d_idx, d_u, d_v, d_w, d_x, d_y, d_z, d_xn, d_yn,
                   d_zn, t, d_uref):
        U = self.U
        d_u[d_idx] = U
        if d_idx == 0:
            d_uref[0] = U
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class CopyOldPU(Equation):
    def initialize(self, d_idx, d_u, d_p, d_oldu, d_oldp):
        d_oldu[d_idx] = d_u[d_idx]
        d_oldp[d_idx] = d_p[d_idx]


class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 30.0  # length of tunnel
        self.Wt = 15.0  # half width of tunnel
        self.nl = 10  # Number of layers for wall/inlet/outlet
        self.io_method = 'donothing'

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=200,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=100,
            help="Number of points in Half width of tunnel."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=1,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=1.0,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--io-method", action="store", type=str, dest="io_method",
            default='donothing', help="'donothing', 'mirror',"
            "or 'characteristic', 'mod_donothing', 'hybrid'."
        )

    def consume_user_options(self):
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.io_method = self.options.io_method
        nx = self.options.nx

        self.nu = 0.0

        self.dx = dx = self.Wt / nx
        self.volume = dx * dx
        hdx = self.options.hdx
        self.nl = (int)(6.0*hdx)

        self.h = h = hdx * self.dx
        dt_cfl = 0.25 * h / (c0 + umax)

        self.dt = min(dt_cfl, 0.001)
        self.tf = 100.0

    def _create_fluid(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0, V=1.0 / volume, u=umax,
            p=1.0, rho=rho, uhat=umax)
        x = fluid.x
        fluid.p[:] = 1.0 - 0.2 *  np.exp(-(x - 0.5)**2/0.001)
        return fluid

    def _create_wall(self):
        dx = self.dx
        h0 = self.h
        x0, y0 = np.mgrid[
            dx/2: self.Lt+self.nl*dx+self.nl*dx: dx, dx/2: self.nl*dx: dx]
        x0 -= self.nl*dx
        y0 -= self.nl*dx+self.Wt
        x0 = np.ravel(x0)
        y0 = np.ravel(y0)

        x1 = np.copy(x0)
        y1 = np.copy(y0)
        y1 += self.nl*dx+2*self.Wt
        x1 = np.ravel(x1)
        y1 = np.ravel(y1)

        x0 = np.concatenate((x0, x1))
        y0 = np.concatenate((y0, y1))
        volume = dx*dx
        wall = get_particle_array(
            name='wall', x=x0, y=y0, m=volume*rho, rho=rho, h=h0,
            V=1.0/volume)
        return wall

    def _set_wall_normal(self, pa):
        props = ['xn', 'yn', 'zn']
        for p in props:
            pa.add_property(p)

        y = pa.y
        cond = y > 0.0
        pa.yn[cond] = 1.0
        cond = y < 0.0
        pa.yn[cond] = -1.0

    def _create_outlet(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx/2:self.nl * dx:dx,  -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x += self.Lt
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        outlet = get_particle_array(
            name='outlet', x=x, y=y, m=m, h=h0, V=1.0/volume, u=umax,
            p=1.0, rho=one * rho, uhat=umax)
        return outlet

    def _create_inlet(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx / 2:self.nl*dx:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x = x - self.nl * dx
        one = np.ones_like(x)
        volume = one * dx * dx

        inlet = get_particle_array(
            name='inlet', x=x, y=y, m=volume * rho, h=h0, u=umax, rho=rho,
            V=1.0 / volume, p=1.0, uhat=umax)
        return inlet

    def create_particles(self):
        fluid = self._create_fluid()
        outlet = self._create_outlet()
        inlet = self._create_inlet()
        wall = self._create_wall()

        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        particles = [fluid, inlet, outlet, wall]
        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        self.scheme.setup_properties(particles)
        self._set_wall_normal(wall)
        for pa in particles:
            pa.add_property('oldu')
            pa.add_property('oldp')
            if not pa.name == 'wall':
                pa.add_output_arrays(['oldp', 'oldu'])

        for pa in particles:
            pa.oldu = pa.u
            pa.oldp = pa.p

        if self.io_method == 'hybrid':
            fluid.uag[:] = umax
            fluid.uta[:] = umax
            fluid.pta[:] = 1.0
            outlet.uta[:] = umax
            outlet.pta[:] = 1.0
            inlet.uta[:] = umax
            inlet.pta[:] = 1.0

        return particles

    def create_scheme(self):
        h = nu = None
        s = EDACScheme(
            ['fluid'], [], dim=2, rho0=rho, c0=c0, h=h, pb=p0,
            nu=nu, alpha=0.1, inlet_outlet_manager=None,
            inviscid_solids=['wall']
        )
        return s

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx)
        scheme.configure(h=self.h, nu=self.nu)
        output_at_times = [0, 0.0336, 0.0548, .1016, .1695]

        scheme.configure_solver(kernel=kernel, tf=self.tf, dt=self.dt,
                                n_damp=0, output_at_times=output_at_times)

    def _get_io_info(self):
        inleteqns = [ResetInletVelocity('ghost_inlet', [], U=-umax, V=0.0,
                     W=0.0),
                     ResetInletVelocity('inlet', [], U=umax, V=0.0,
                     W=0.0),
                     CopyOldPU('fluid', sources=None)]
        i_update_cls = None
        i_has_ghost = True
        o_update_cls = None
        o_has_ghost = True
        manager = None
        nl = 1
        print(self.io_method)
        if self.io_method == 'donothing':
            from donothing.inlet import Inlet
            from donothing.outlet import Outlet
            from donothing.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'mirror':
            from mirror.inlet import Inlet
            from mirror.outlet import Outlet
            from mirror.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'hybrid':
            from hybrid.inlet import Inlet
            from hybrid.outlet import Outlet
            from hybrid.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            o_has_ghost = False
            manager = SimpleInletOutlet
            nl = 5
        elif self.io_method == 'characteristic':
            from characteristic.inlet import Inlet
            from characteristic.outlet import Outlet
            from characteristic.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'mod_donothing':
            from mod_donothing.inlet import Inlet
            from mod_donothing.outlet import Outlet
            from mod_donothing.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[0.0, 0.0, 0.0], equations=inleteqns,
            has_ghost=i_has_ghost, update_cls=i_update_cls, umax=umax
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt, 0.0, 0.0], has_ghost=o_has_ghost,
            update_cls=o_update_cls, nl=nl
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        t, ut, pt, x0= self._plot_pu_vs_t()
        res = os.path.join(self.output_dir, 'results.npz')
        np.savez(res, t=t, pt=pt, ut=ut, x=x0)

    def _plot_pu_vs_t(self):
        from pysph.solver.utils import load, iter_output
        from pysph.tools.interpolator import Interpolator

        data = load(self.output_files[0])
        fluid = data['arrays']['fluid']
        inlet = data['arrays']['inlet']
        outlet = data['arrays']['outlet']

        x0 = np.linspace(-0.05, 1.05, 100)
        y0 = np.zeros_like(x0)

        interp7 = Interpolator(list([fluid, inlet, outlet]), x=x0, y=y0,
                               method='order1')

        ut, pt, t = [], [], []
        import gc
        for sd, arrays in iter_output(self.output_files):
            fluid = arrays['fluid']
            outlet = arrays['outlet']
            fluid.u[:] = fluid.oldu[:]
            fluid.p[:] = fluid.oldp[:]
            ti = sd['t']
            t.append(ti)
            inlet = arrays['inlet']
            interp7.update_particle_arrays([fluid, inlet, outlet])
            u = interp7.interpolate('u')
            p = interp7.interpolate('p')
            ut.append(u)
            pt.append(p)
            gc.collect()

        t, ut, pt = list(map(np.asarray, (t, ut, pt)))
        return t, ut, pt, x0

    def customize_output(self):
        if self.io_method == 'hybrid':
            self._mayavi_config('''
            viewer.scalar = 'u'
            ''')
        elif self.io_method == 'mirror':
            self._mayavi_config('''
            viewer.scalar = 'u'
            parr = ['ghost_outlet', 'ghost_inlet']
            for particle in parr:
                b = particle_arrays[particle]
                b.visible = False
            ''')
        else:
            self._mayavi_config('''
            viewer.scalar = 'u'
            parr = ['ghost_inlet']
            for particle in parr:
                b = particle_arrays[particle]
                b.visible = False
            ''')


if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
