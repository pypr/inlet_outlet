"""
Backward facing step
"""
import numpy as np
import os

from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G

# local imports
from inlet_outlet_manager import InletInfo, OutletInfo
from edac import EDACScheme

# Fluid mechanical/numerical parameters
rho = 1.225
gamma = 7.0
alpha = 0.2
beta = 0.0


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W, hstep, btime, hinlet):
        self.U = U
        self.V = V
        self.W = W

        self.hinlet = hinlet
        self.hstep = hstep
        # the secondary profile base width use to
        # slowly make the velocity profile at inlet parabolic
        self.vdist = hinlet * 2
        self.btime = btime

        super(ResetInletVelocity, self).__init__(dest, sources)

    def loop(self, d_idx, d_u, d_v, d_w, d_x, d_y, d_z, d_xn, d_yn, d_zn,
             t):
        if (t > self.btime):
            time = self.btime
        else:
            time = t
        y = d_y[d_idx]
        L = self.hinlet
        mid = (L + 2 * self.hstep) * 0.5
        yup = (self.hstep + L + self.vdist) -\
            (time * self.vdist/self.btime)
        ydn = (self.hstep - self.vdist) +\
            (time * self.vdist/self.btime)
        deno = (mid-ydn)*(yup-mid)
        profile = (y - ydn)*(yup-y)/deno
        d_u[d_idx] = self.U # * profile
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class BackwardFacingStep(Application):
    def initialize(self):
        self.Lt = 0.8
        self.h_step = 0.0049
        self.h_inlet = 0.0052
        self.l_inlet = 0.25*self.Lt
        self.nl = 5.0
        self.io_method = 'donothing'

    def add_user_options(self, group):
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=49,
            help="Number of points in the step."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=0.15,
            help="Length of the tunnel."
        )
        group.add_argument(
            "--h-step", action="store", type=float, dest="h_step",
            default=0.0049, help="Height of the step."
        )
        group.add_argument(
            "--re", action="store", type=float, dest="re",
            default=389, help="Reynolds number."
        )
        group.add_argument(
            "--btime", action="store", type=float, dest="btime",
            default=0.0, help="Buffer time to make the profile"
            " parabolic from constant."
        )
        group.add_argument(
            "--io-method", action="store", type=str, dest="io_method",
            default='donothing', help="'donothing', 'mirror',"
            "or 'characteristic'."
        )

    def consume_user_options(self):
        nx = self.options.nx
        self.Lt = self.options.Lt
        self.h_step = self.options.h_step
        re = self.options.re
        self.dx = self.dx = self.h_step / nx
        self.hdx = self.options.hdx
        self.btime = self.options.btime
        self.io_method = self.options.io_method

        self.h_inlet = 0.0052
        self.l_inlet = 0.25*self.Lt
        self.nl = (int)(5.0 * self.hdx)
        self.nlo = 3*(int)(5.0 * self.hdx)
        self.volume = self.dx * self.dx

        self.nu = 1.47e-5
        self.umax = re * self.nu / (2 * self.h_inlet)
        self.c0 = c0 = 10 * self.umax
        self.p0 = rho * c0 * c0

        h0 = self.hdx * self.dx
        self.dt_cfl = 0.25 * h0 / (c0 + self.umax)
        self.dt_viscous = 0.125 * h0**2 / self.nu

        self.dt = min(self.dt_cfl, self.dt_viscous)
        self.tf = 100.0

    def _create_fluid(self):
        h0 = self.hdx * self.dx
        x = np.arange(self.dx/2, self.Lt, self.dx)
        y = np.arange(self.h_step + self.dx/2, self.h_inlet + self.h_step,
                      self.dx)
        x, y = np.meshgrid(x, y)
        xf1 = x.ravel()
        yf1 = y.ravel()

        # Block2 fluids
        x = np.arange(self.l_inlet + self.dx/2, self.Lt, self.dx)
        y = np.arange(self.dx/2, self.h_step, self.dx)
        x, y = np.meshgrid(x, y)
        xf2 = x.ravel()
        yf2 = y.ravel()

        # All fluidsself.dx/2
        x = np.concatenate((xf1, xf2))
        y = np.concatenate((yf1, yf2))

        one = np.ones_like(x)
        volume = self.dx * self.dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0 * one, V=1.0 / volume,
            u=self.umax * one, p=0.0 * one, rho=one * rho)
        return fluid

    def _create_outlet(self):
        h0 = self.hdx * self.dx
        y = np.arange(self.dx/2, self.h_inlet + self.h_step, self.dx)
        x = np.arange(self.dx/2, self.nlo*self.dx, self.dx)
        x = x + self.Lt
        x, y = np.meshgrid(x, y)
        y = y.ravel()
        x = x.ravel()

        one = np.ones_like(x)
        volume = self.dx * self.dx * one
        m = volume * rho
        outlet = get_particle_array(
            name='outlet', x=x, y=y, m=m, h=h0 * one, V=1.0 / volume,
            u=self.umax * one, uhat=self.umax, p=0.0 * one, rho=one * rho)
        return outlet

    def _create_inlet(self):
        h0 = self.hdx * self.dx
        y = np.arange(self.h_step+self.dx/2, self.h_inlet + self.h_step,
                      self.dx)
        x = np.arange(self.dx/2, self.nl*self.dx, self.dx)
        x = x - self.nl*self.dx
        x, y = np.meshgrid(x, y)
        y = y.ravel()
        x = x.ravel()

        one = np.ones_like(x)
        volume = one * self.dx * self.dx
        m = volume * rho
        inlet = get_particle_array(
            name='inlet', x=x, y=y, m=m, h=h0 * one, u=self.umax * one, rho=rho
            * one, V=1.0 / volume, p=0.0 * one)
        return inlet

    def _create_wall(self):
        h0 = self.hdx * self.dx
        x = np.arange(-self.nl*self.dx + self.dx/2, self.Lt +
                      self.nlo*self.dx, self.dx)
        y = np.arange(-self.nl*self.dx+self.dx/2, self.h_step +
                      self.h_inlet + self.nl*self.dx, self.dx)
        x, y = np.meshgrid(x, y)
        y = y.ravel()
        x = x.ravel()

        cond = (x - self.Lt * 0.8 < 1e-14) & (x - self.Lt * 0.04 > 1e-14)
        x = x[cond]
        y = y[cond]

        one = np.ones_like(x)
        volume = self.dx * self.dx * one
        m = volume * rho
        wall = get_particle_array(
            name='wall', m=m, x=x, y=y, h=h0 * one, V=41.0 / volume, u=0.0 *
            one, p=0.0 * one, rho=one * rho)
        return wall


    def _create_wall_inviscid(self):
        h0 = self.hdx * self.dx
        x = np.arange(-self.nl*self.dx + self.dx/2, self.Lt +
                      self.nlo*self.dx, self.dx)
        y = np.arange(-self.nl*self.dx+self.dx/2, self.h_step +
                      self.h_inlet + self.nl*self.dx, self.dx)
        x, y = np.meshgrid(x, y)
        y = y.ravel()
        x = x.ravel()

        cond = (x - self.Lt * 0.8 > 1e-14) | (x - self.Lt * 0.04 < 1e-14)
        x = x[cond]
        y = y[cond]

        cond1 = y - 0.5 * (self.h_step + self.h_inlet) > 1e-14
        xn = np.zeros_like(x)
        yn = np.zeros_like(y)
        xn[:] = 0.0
        yn[cond1] = -1.0
        yn[~cond1] = 1.0

        one = np.ones_like(x)
        volume = self.dx * self.dx * one
        m = volume * rho
        wall = get_particle_array(
            name='inviscid_wall', m=m, x=x, y=y, h=h0 * one, V=41.0 / volume, u=0.0 *
            one, p=0.0 * one, rho=one * rho, xn=xn, yn=yn)
        return wall

    def create_particles(self):
        fluid = self._create_fluid()
        wall = self._create_wall()
        inviscid_wall = self._create_wall_inviscid()
        outlet = self._create_outlet()
        inlet = self._create_inlet()
        G.remove_overlap_particles(wall, inlet, self.dx, dim=2)
        G.remove_overlap_particles(wall, outlet, self.dx, dim=2)
        G.remove_overlap_particles(wall, fluid, self.dx, dim=2)

        G.remove_overlap_particles(inviscid_wall, inlet, self.dx, dim=2)
        G.remove_overlap_particles(inviscid_wall, outlet, self.dx, dim=2)
        G.remove_overlap_particles(inviscid_wall, fluid, self.dx, dim=2)

        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)
        particles = [fluid, inlet, outlet, wall, inviscid_wall]
        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        self.scheme.setup_properties(particles)
        if self.io_method == 'characteristic':
            for pa_arr in particles:
                if pa_arr.name not in ['wall']:
                    pa_arr.uold[:] = self.umax
                    pa_arr.uhold[:] = self.umax
                    pa_arr.uhat[:] = self.umax

        return particles

    def create_scheme(self):
        h0 = nu = None
        c0 = p0 = None
        s = EDACScheme(
            ['fluid'], ['wall'], dim=2, rho0=rho, c0=c0, h=h0,
            pb=p0, nu=nu, inlet_outlet_manager=None,
            inviscid_solids=['inviscid_wall']
        )
        return s

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        h0 = self.hdx * self.dx
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx)
        scheme.configure(h=h0, nu=self.nu, c0=self.c0, pb=self.p0)

        scheme.configure_solver(kernel=kernel, tf=self.tf, dt=self.dt,
                                pfreq=pfreq, n_damp=100)

    def _get_io_info(self):
        inleteqns = [ResetInletVelocity('ghost_inlet', [], U=-self.umax, V=0.0,
                     W=0.0, hstep=self.h_step, hinlet=self.h_inlet,
                     btime=self.btime),
                     ResetInletVelocity('inlet', [], U=self.umax, V=0.0,
                     W=0.0, hstep=self.h_step, hinlet=self.h_inlet,
                     btime=self.btime)]
        i_update_cls = None
        i_has_ghost = True
        o_update_cls = None
        o_has_ghost = True
        manager = None
        print(self.io_method)
        if self.io_method == 'donothing':
            from donothing.inlet import Inlet
            from donothing.outlet import Outlet
            from donothing.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'mirror':
            from mirror.inlet import Inlet
            from mirror.outlet import Outlet
            from mirror.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'hybrid':
            from hybrid.inlet import Inlet
            from hybrid.outlet import Outlet
            from hybrid.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            o_has_ghost = False
            manager = SimpleInletOutlet
            nl = 5.0
        if self.io_method == 'mod_donothing':
            from mod_donothing.inlet import Inlet
            from mod_donothing.outlet import Outlet
            from mod_donothing.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'characteristic':
            from characteristic.inlet import Inlet
            from characteristic.outlet import Outlet
            from characteristic.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[0.0, 0.0, 0.0], equations=inleteqns,
            has_ghost=i_has_ghost, update_cls=i_update_cls
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt, 0.0, 0.0], has_ghost=o_has_ghost,
            update_cls=o_update_cls
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        return iom.get_inlet_outlet(particle_arrays)

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        # frac = [2.55, 4.18, 7.76, 13.57]
        frac = [2.55, 3.57, 4.80, 7.14]
        rl, x, y0, y1, u0, p0, u1, p1, u2, p2, u3, p3 = self._get_p_v(frac)
        res = os.path.join(self.output_dir, 'results.npz')
        np.savez(res, rl=rl, x=x, y0=y0, y1=y1, u0=u0, p0=p0, u1=u1, p1=p1,
                 u2=u2, p2=p2, u3=u3, p3=p3)

    def _get_p_v(self, frac):
        print('postprocessing')
        from pysph.solver.utils import load
        from pysph.tools.interpolator import Interpolator

        lastfile = len(self.output_files) - 1
        data = load(self.output_files[lastfile])

        hi = self.h_inlet
        hs = self.h_step
        S = self.h_step
        Si = self.l_inlet
        L = self.Lt
        fluid = data['arrays']['fluid']

        y0 = np.linspace(0.0, (hi+hs), 100)
        x0 = np.ones_like(y0) * frac[0] * S + Si
        interp = Interpolator([fluid], x=x0, y=y0,
                              method='order1')
        u0 = interp.interpolate('u')
        p0 = interp.interpolate('p')

        y1 = np.linspace(0.0, (hi+hs), 100)
        x1 = np.ones_like(y1) * frac[1] * S + Si
        interp = Interpolator([fluid], x=x1, y=y1,
                              method='order1')
        u1 = interp.interpolate('u')
        p1 = interp.interpolate('p')

        x2 = np.ones_like(y1) * frac[2] * S + Si
        interp = Interpolator([fluid], x=x2, y=y1,
                              method='order1')
        u2 = interp.interpolate('u')
        p2 = interp.interpolate('p')

        x3 = np.ones_like(y1) * frac[3] * S + Si
        interp = Interpolator([fluid], x=x3, y=y1,
                              method='order1')
        u3 = interp.interpolate('u')
        p3 = interp.interpolate('p')

        x4 = np.linspace(0.3*L, L, 1000)
        y4 = np.zeros_like(x4) + self.dx / 2
        interp = Interpolator([fluid], x=x4, y=y4,
                              method='order1')
        u4 = interp.interpolate('u')
        rl = 0.0
        for i, u in enumerate(u4):
            if u > 0.00001:
                rl = x4[i] - Si
                break

        import matplotlib
        matplotlib.use('Agg')

        from matplotlib import pyplot as plt
        plt.plot(u1, y1, 'k-', label='x=hstep')
        plt.xlabel('u/U')
        plt.ylabel('y/H')
        plt.legend()
        fig = os.path.join(self.output_dir, 'u_profile.pdf')
        plt.savefig(fig)
        plt.close()

        plt.plot(p1, y1, 'k-', label='x=hstep')
        plt.xlabel('p/P')
        plt.ylabel('y/H')
        plt.legend()
        fig = os.path.join(self.output_dir, 'p_profile.pdf')
        plt.savefig(fig)
        plt.close()

        return np.array([rl]), np.array(frac), y0, y1, u0, p0, u1, p1,\
            u2, p2, u3, p3

if __name__ == '__main__':
    app = BackwardFacingStep()
    app.run()
    app.post_process(app.info_filename)
