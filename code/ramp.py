"""
2D wave
"""
import numpy as np
import os
from math import exp, cos

from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application

# local files
from edac import EDACScheme
from inlet_outlet_manager import (
        InletInfo, OutletInfo)

# Fluid mechanical/numerical parameters
rho = 1000
umax = 0.0
c0 = 10.0
p0 = rho * c0 * c0


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W):
        self.U = U
        self.V = V
        self.W = W

        super(ResetInletVelocity, self).__init__(dest, sources)

    def initialize(self, d_idx, d_u, d_v, d_w, d_x, d_y, d_z, d_xn, d_yn,
                   d_zn, t, d_uref):
        c = t
        if t > 1.0:
            c = 1.0
        U = self.U / (abs(self.U))*(c)
        if d_idx == 0:
            d_uref[0] = U
        d_u[d_idx] = U
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class CopyOldPU(Equation):
    def initialize(self, d_idx, d_u, d_p, d_oldu, d_oldp):
        d_oldu[d_idx] = d_u[d_idx]
        d_oldp[d_idx] = d_p[d_idx]


class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 30.0  # length of tunnel
        self.Wt = 15.0  # half width of tunnel
        self.nl = 10  # Number of layers for wall/inlet/outlet
        self.io_method = 'donothing'

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=200,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=40,
            help="Number of points in Half width of tunnel."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=1,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=2.0,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--io-method", action="store", type=str, dest="io_method",
            default='donothing', help="'donothing', 'mirror',"
            "or 'characteristic', 'mod_donothing', 'hybrid'."
        )

    def consume_user_options(self):
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.io_method = self.options.io_method
        nx = self.options.nx

        self.nu = 0.0

        self.dx = dx = self.Wt / nx
        self.volume = dx * dx
        hdx = self.options.hdx
        self.nl = (int)(6.0*hdx)

        self.h = h = hdx * self.dx
        dt_cfl = 0.25 * h / (c0 + umax)

        self.dt = dt_cfl
        self.tf = 100.0

    def _create_fluid(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0, V=1.0 / volume, u=umax,
            p=0.0, rho=rho, uhat=umax)
        return fluid

    def _create_wall(self):
        dx = self.dx
        h0 = self.h
        x0, y0 = np.mgrid[
            dx/2: self.Lt+self.nl*dx+self.nl*dx: dx, dx/2: self.nl*dx: dx]
        x0 -= self.nl*dx
        y0 -= self.nl*dx+self.Wt
        x0 = np.ravel(x0)
        y0 = np.ravel(y0)

        x1 = np.copy(x0)
        y1 = np.copy(y0)
        y1 += self.nl*dx+2*self.Wt
        x1 = np.ravel(x1)
        y1 = np.ravel(y1)

        x0 = np.concatenate((x0, x1))
        y0 = np.concatenate((y0, y1))
        volume = dx*dx
        wall = get_particle_array(
            name='wall', x=x0, y=y0, m=volume*rho, rho=rho, h=h0,
            V=1.0/volume)
        return wall

    def _set_wall_normal(self, pa):
        props = ['xn', 'yn', 'zn']
        for p in props:
            pa.add_property(p)

        y = pa.y
        cond = y > 0.0
        pa.yn[cond] = 1.0
        cond = y < 0.0
        pa.yn[cond] = -1.0

    def _create_outlet(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx/2:self.nl * dx:dx,  -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x += self.Lt
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        outlet = get_particle_array(
            name='outlet', x=x, y=y, m=m, h=h0, V=1.0/volume, u=umax,
            p=0.0, rho=one * rho, uhat=umax)
        return outlet

    def _create_inlet(self):
        dx = self.dx
        h0 = self.h
        x, y = np.mgrid[dx / 2:self.nl*dx:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x = x - self.nl * dx
        one = np.ones_like(x)
        volume = one * dx * dx

        inlet = get_particle_array(
            name='inlet', x=x, y=y, m=volume * rho, h=h0, u=umax, rho=rho,
            V=1.0 / volume, p=0.0, uhat=umax)
        return inlet

    def create_particles(self):
        fluid = self._create_fluid()
        outlet = self._create_outlet()
        inlet = self._create_inlet()
        wall = self._create_wall()

        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        particles = [fluid, inlet, outlet, wall]
        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        self.scheme.setup_properties(particles)
        self._set_wall_normal(wall)
        for pa in particles:
            pa.add_property('oldu')
            pa.add_property('oldp')
            if not pa.name == 'wall':
                pa.add_output_arrays(['oldp', 'oldu'])

        for pa in particles:
            pa.oldu = pa.u
            pa.oldp = pa.p

        if self.io_method == 'hybrid':
            fluid.uag[:] = umax
            fluid.uta[:] = umax
            outlet.uta[:] = umax
            inlet.uta[:] = umax

        return particles

    def create_scheme(self):
        h = nu = None
        s = EDACScheme(
            ['fluid'], [], dim=2, rho0=rho, c0=c0, h=h, pb=p0,
            nu=nu, alpha=0.0, inlet_outlet_manager=None,
            inviscid_solids=['wall']
        )
        return s

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx)
        scheme.configure(h=self.h, nu=self.nu)

        scheme.configure_solver(kernel=kernel, tf=self.tf, dt=self.dt,
                                pfreq=pfreq, n_damp=0)

    def _get_io_info(self):
        inleteqns = [ResetInletVelocity('ghost_inlet', [], U=-1.0, V=0.0,
                     W=0.0),
                     ResetInletVelocity('inlet', [], U=1.0, V=0.0,
                     W=0.0),
                     CopyOldPU('fluid', sources=None)]
        i_update_cls = None
        i_has_ghost = True
        o_update_cls = None
        o_has_ghost = True
        manager = None
        nl = 1
        print(self.io_method)
        if self.io_method == 'donothing':
            from donothing.inlet import Inlet
            from donothing.outlet import Outlet
            from donothing.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'mirror':
            from mirror.inlet import Inlet
            from mirror.outlet import Outlet
            from mirror.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'hybrid':
            from hybrid.inlet import Inlet
            from hybrid.outlet import Outlet
            from hybrid.simple_inlet_outlet import SimpleInletOutlet
            i_update_cls = Inlet
            o_update_cls = Outlet
            o_has_ghost = False
            manager = SimpleInletOutlet
        elif self.io_method == 'characteristic':
            from characteristic.inlet import Inlet
            from characteristic.outlet import Outlet
            from characteristic.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet
        elif self.io_method == 'mod_donothing':
            from mod_donothing.inlet import Inlet
            from mod_donothing.outlet import Outlet
            from mod_donothing.simple_inlet_outlet import SimpleInletOutlet
            o_has_ghost = False
            i_update_cls = Inlet
            o_update_cls = Outlet
            manager = SimpleInletOutlet

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[0.0, 0.0, 0.0], equations=inleteqns,
            has_ghost=i_has_ghost, update_cls=i_update_cls, umax=0.0
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt, 0.0, 0.0], has_ghost=o_has_ghost,
            update_cls=o_update_cls, nl=nl
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        t, p, u, px, v, ux, vx, x1, y1, ut, pt, utx, ptx = self._plot_pu_vs_t()
        res = os.path.join(self.output_dir, 'results.npz')
        np.savez(res, t=t, p=p, u=u, x1=x1, y1=y1, ut=ut, pt=pt, utx=utx,
                 ptx=ptx, px=px, v=v, ux=ux, vx=vx)

    def _plot_pu_vs_t(self):
        from pysph.solver.utils import load, iter_output
        from pysph.tools.interpolator import Interpolator

        data = load(self.output_files[0])
        fluid = data['arrays']['fluid']
        inlet = data['arrays']['inlet']
        outlet = data['arrays']['outlet']

        y0 = [0.0]
        x0 = np.ones_like(y0) * 1.9
        y1 = [0.0]
        x1 = np.ones_like(y0) * 1.8
        y2 = [0.0]
        x2 = np.ones_like(y0) * 1.7
        y3 = [0.5]
        x3 = np.ones_like(y0) * 1.9
        y4 = [0.5]
        x4 = np.ones_like(y0) * 1.8
        y5 = [0.5]
        x5 = np.ones_like(y0) * 1.7

        x7 = np.linspace(-self.nl*self.dx, self.Lt+self.nl*self.dx, 200)
        y7 = np.zeros_like(x1)

        x = [x0, x1, x2, x3, x4, x5]
        y = [y0, y1, y2, y3, y4, y5]
        interps = []
        for i, xi in enumerate(x):
            interps.append(Interpolator(list([fluid]), x=x[i], y=y[i],
                                        method='order1'))

        interp7 = Interpolator(list([fluid, inlet, outlet]), x=x7, y=y7,
                               method='order1')

        t, p, u, v, px, ux, vx = [], [], [], [], [], [], []
        for i in range(6):
            u.append([])
            v.append([])
            p.append([])
            px.append([])
            ux.append([])
            vx.append([])
        ut, pt, utx, ptx = [], [], [], []
        do_once = True
        import gc
        for sd, arrays in iter_output(self.output_files):
            fluid = arrays['fluid']
            outlet = arrays['outlet']
            fluid.u[:] = fluid.oldu[:]
            fluid.p[:] = fluid.oldp[:]
            ti = sd['t']
            t.append(ti)
            if (ti > 1.0) and (do_once):
                inlet = arrays['inlet']
                interp7.update_particle_arrays([fluid, inlet, outlet])
                ut = interp7.interpolate('u')
                pt = interp7.interpolate('p')
                utx = interp7.interpolate('u', comp=1)
                ptx = interp7.interpolate('p', comp=1)
                do_once = False
            for i, interp in enumerate(interps):
                interp.update_particle_arrays([fluid])
                u[i].append(interp.interpolate('u'))
                v[i].append(interp.interpolate('v'))
                p[i].append(interp.interpolate('p'))
                px[i].append(interp.interpolate('p', comp=1))
                ux[i].append(interp.interpolate('u', comp=1))
                vx[i].append(interp.interpolate('v', comp=1))
            gc.collect()

        t, p, u, v, px, ux, vx = list(map(np.asarray, (t, p, u, v, px, ux,
                                      vx)))
        # Now plot the results.
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, 2*p[1]/rho/1.5**2, '--', label='p at (%s, %s)'
                 % (x[1][0], y[1][0]))
        plt.plot(t, u[1]/1.5, '-', label="u at (%s, %s)"
                 % (x[1][0], y[1][0]))
        plt.xlabel(r'$t$')
        plt.ylabel('Pressure/u-Velocity')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "reflection.png")
        plt.savefig(fig, dpi=300)
        plt.close()
        return t, p, u, px, v, ux, vx, x7, y7, ut, pt, utx, ptx

    def customize_output(self):
        if self.io_method == 'hybrid':
            self._mayavi_config('''
            viewer.scalar = 'u'
            ''')
        elif self.io_method == 'mirror':
            self._mayavi_config('''
            viewer.scalar = 'u'
            parr = ['ghost_outlet', 'ghost_inlet']
            for particle in parr:
                b = particle_arrays[particle]
                b.visible = False
            ''')
        else:
            self._mayavi_config('''
            viewer.scalar = 'u'
            parr = ['ghost_inlet']
            for particle in parr:
                b = particle_arrays[particle]
                b.visible = False
            ''')


if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
