#!/usr/bin/env python
import os
from itertools import cycle, product

from automan.api import PySPHProblem as Problem
from automan.api import (Automator, Simulation, filter_cases,
                         )
from pysph.solver.utils import load
import numpy as np
import matplotlib
from scipy.signal import savgol_filter
matplotlib.use('agg')


backend = ' --openmp '
IO_NAMES = ['donothing', 'mirror', 'hybrid', 'mod_donothing', 'characteristic']
INAMES = ['do-nothing', 'mirror', 'hybrid', 'New do-nothing', 'characteristic']
#LINESTYLE = cycle(['r--', 'b:', 'g-.', 'c--', 'm--', 'k--'])
LINESTYLE = cycle([
     (0, (1, 1)),
     (0, (5, 1)),
     (0, (5, 1, 1, 1)),

     (0, (5, 1, 1, 1, 1, 1)),
     (0, (5, 1, 1, 1, 1, 1, 1, 1)),
     (0, (5, 5))])
COLOR = cycle(['r', 'b', 'g', 'c', 'm', 'k'])

def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    else:
        return params


def get_files_at_given_times(files, times):
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if count >= len(times):
            break
        if abs(t - times[count]) < t*1e-8:
            result.append(f)
            count += 1
        elif t > times[count]:
            result.append(f)
            count += 1
    return result


def get_files_at_given_times_from_log(files, times, logfile):
   import re
   result = []
   time_pattern = r"output at time\ (\d+(?:\.\d+)?)"
   file_count, time_count = 0, 0
   with open(logfile, 'r') as f:
       for line in f:
           if time_count >= len(times):
               break
           t = re.findall(time_pattern, line)
           if t:
               if float(t[0]) in times:
                   result.append(files[file_count])
                   time_count += 1
               elif float(t[0]) > times[time_count]:
                   result.append(files[file_count])
                   time_count += 1
               file_count += 1
   return result


def make_table(column_names, row_data, output_fname, sort_cols=None,
               **extra_kw):
    import pandas as pd
    col_data = [[] for i in range(len(column_names))]
    for row in row_data:
        for i, item in enumerate(row):
            col_data[i].append(item)
    d = {n: col_data[i] for i, n in enumerate(column_names)}
    df = pd.DataFrame(d, columns=column_names)
    if sort_cols:
        sort_by = [column_names[i] for i in sort_cols]
    else:
        sort_by = column_names[-1]
    df.sort_values(by=sort_by, inplace=True)
    parent = os.path.dirname(output_fname)
    if not os.path.exists(parent):
        os.mkdir(parent)
    with open(output_fname, 'w') as fp:
        fp.write(df.to_latex(index=False, escape=False, **extra_kw))


class FlowPastCylinder(Problem):
    def get_name(self):
        return 'flow_past_cylinder'

    def normalize_u(self, u):
        return u / self.umax

    def normalize_p(self, p):
        return p / (0.5 * self.rho * self.umax**2)

    def setup(self):
        self.plotdata = {}
        self.cl = {}
        self.cd = {}
        self.st = {}
        get_path = self.input_path
        self.umax = 1.0
        self.rho = 1000
        self.re = re = [200]
        self.dc = 2.0
        self.nx = 30
        self.tf = 200
        pfreq = 400
        cmd = 'python code/flow_past_cylinder_2d.py ' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'mod_donothing': dict(io_method='mod_donothing'),
            'characteristic': dict(io_method='characteristic'),
        }

        self.case_info = {
            f'{method}_re_{r}_nx_30': dict(
                re=r, **_case_info[method],
                dc=self.dc, nx=self.nx, tf=self.tf, pfreq=self.nx*10
            )
            for method, r in product(_case_info, re)
        }

        # cases with different reynolds number
        self.case_info['hybrid_re_20_nx_40'] = dict(
                re=20, io_method='hybrid',
                dc=self.dc, nx=40, tf=100, pfreq=400
            )
        self.case_info['mod_donothing_re_20_nx_40'] = dict(
                re=20, io_method='mod_donothing',
                dc=self.dc, nx=40, tf=100, pfreq=400
            )
        self.case_info['hybrid_re_20_nx_20'] = dict(
                re=20, io_method='hybrid',
                dc=self.dc, nx=20, tf=100, pfreq=200
            )
        self.case_info['hybrid_re_20_nx_30'] = dict(
                re=20, io_method='hybrid',
                dc=self.dc, nx=30, tf=100, pfreq=300
            )
        self.case_info['mod_donothing_re_20_nx_20'] = dict(
                re=20, io_method='mod_donothing',
                dc=self.dc, nx=20, tf=100, pfreq=200
            )
        self.case_info['mod_donothing_re_20_nx_30'] = dict(
                re=20, io_method='mod_donothing',
                dc=self.dc, nx=30, tf=100, pfreq=300
            )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=12, n_thread=48),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._plot_cl_all_methods_re_200()
        self._get_cdcl_values()
        self._get_St_values()
        self._plot_convergence_re20()
        self._plot_cd_cl_hybrid_all()
        self._table_compare_cd_cl_st_all()
        self._plot_particles()

    def _plot_cl_all_methods_re_200(self):
        import matplotlib.pyplot as plt
        cdcldata = {}
        cases = filter_cases(self.cases, re=200)
        for case in cases:
            name = case.params['io_method']
            if not cdcldata.get(name):
                cdcldata[name] = {}
            data = case.data
            nx = case.params['nx']
            cdcldata[name]['t'] = data['t']
            cdcldata[name]['cl'] = np.convolve(data['cl'], np.ones((5,))/5, mode='same')

        ioname = IO_NAMES
        fig = plt.figure(figsize=(8, 8))
        grid = plt.GridSpec(5, 4, hspace=0.5, wspace=0.5)
        ax = []

        ax.append(fig.add_subplot(grid[0, 0:4]))
        ax[-1].xaxis.set_tick_params(labelbottom=False)
        ax.append(fig.add_subplot(grid[1, 0:4]))
        ax[-1].xaxis.set_tick_params(labelbottom=False)
        ax.append(fig.add_subplot(grid[2, 0:4]))
        ax[-1].xaxis.set_tick_params(labelbottom=False)
        ax.append(fig.add_subplot(grid[3, 0:4]))
        ax[-1].xaxis.set_tick_params(labelbottom=False)
        ax.append(fig.add_subplot(grid[4, 0:4]))
        for i, axes in enumerate(ax):
            t = cdcldata[ioname[i]]['t']
            cl = cdcldata[ioname[i]]['cl']
            axes.plot(t, cl, '-k', linewidth=1.0)
            axes.set_ylabel('$c_l$', va='bottom')
            if i == len(ax) - 1:
                axes.set_xlabel('t')
            axes.grid(True)
            axes.set_xlim([0, 200])
            axes.set_ylim([-1.0, 1.0])
            axes.set_title(INAMES[i].title())

        plt.savefig(self.output_path('cl_comp.png'), dpi=300)
        plt.close()

    def _plot_cd_cl_hybrid_all(self):
        import matplotlib.pyplot as plt
        cdcldata = {}
        cases0 = filter_cases(self.cases, io_method='hybrid', nx=30)
        cases1 = filter_cases(self.cases, io_method='mod_donothing', nx=30)
        cases = np.concatenate((cases0, cases1))
        for case in cases:
            re = case.params['re']
            ioname = case.params['io_method']
            if not cdcldata.get(re):
                cdcldata[re] = {}
            data = case.data
            cdcldata[re]['t' + ioname] = data['t']
            cdcldata[re]['cd' + ioname] = np.convolve(data['cd'], np.ones((5,))/5, mode='same')
            cdcldata[re]['cl' + ioname] = np.convolve(data['cl'], np.ones((5,))/5, mode='same')

        re = [20, 200, 20, 200]
        fig = plt.figure(figsize=(8, 8))
        grid = plt.GridSpec(2, 2, hspace=0.3, wspace=0.5)
        ax = []

        ax.append(fig.add_subplot(grid[0, 0:1]))
        ax[-1].xaxis.set_tick_params(labelbottom=False)
        ax.append(fig.add_subplot(grid[1, 0:1]))
        ax.append(fig.add_subplot(grid[0, 1:2]))
        ax[-1].xaxis.set_tick_params(labelbottom=False)
        ax.append(fig.add_subplot(grid[1, 1:2]))
        for i, axes in enumerate(ax):
            name = 'hybrid'
            if i > 1:
                name = 'mod_donothing'
            t = cdcldata[re[i]]['t'+ name]
            cd = cdcldata[re[i]]['cd'+ name]
            cl = cdcldata[re[i]]['cl'+ name]
            axes.plot(t[0:-5], cd[0:-5], '-r', linewidth=1.0, label=r'$c_d$')
            axes.plot(t, cl, '-g', linewidth=1.0, label=r'$c_l$')
            axes.set_xlabel('t')
            axes.grid(True)
            if (i == 0 or i == 2):
                axes.legend()
            if name == 'mod_donothing':
                name = 'New do-nothing'
            name = name.title()
            if re[i] == 20:
                axes.set_title(name + '\nRe = ' + str(re[i]))
                axes.set_ylim([-0.2, 3.0])
            else:
                axes.set_title('Re = ' + str(re[i]))

        plt.savefig(self.output_path('re.png'), dpi=300)
        plt.close()

    def _plot_convergence_re20(self):
        import matplotlib.pyplot as plt
        _id0 = 're_' + str(20) + '_nx_' + str(20)
        _id1 = 're_' + str(20) + '_nx_' + str(30)
        _id2 = 're_' + str(20) + '_nx_' + str(40)
        ids = [_id0, _id1, _id2]
        dx = [2./20, 2.0/30, 2./40]
        column_names = ['Particle Spacing', 'Hybrid', 'New do-nothing']

        rows = []

        for i, _id in enumerate(ids):
            row = ['%.2f'%dx[i]]
            row.append(r'$%.3f$'%self.cd['hybrid'][_id])
            row.append(r'$%.3f$'%self.cd['mod_donothing'][_id])
            rows.append(row)

        make_table(
            column_names, rows, self.output_path(self.get_name() +
                '_conv.tex'),
            sort_cols=[0, -1], column_format='lrrr'
        )

    def _get_St_values(self):
        D = self.dc
        U = self.umax

        for case in self.cases:
            name = case.params['io_method']
            re = case.params['re']
            nx = case.params['nx']
            _id = 're_' + str(re) + '_nx_' + str(nx)
            if not self.st.get(name):
                self.st[name] = {}
            data = case.data
            t = data['t']
            cl = data['cl']
            cond = t > 0  # to change
            t_trunc = t[cond]
            cl_trunc = cl[cond]
            fft = np.fft.fft(cl_trunc)
            ith_freq = np.argmax(abs(fft))
            freq = np.fft.fftfreq(
                t_trunc.shape[-1], t_trunc[1]-t_trunc[0])
            f = freq[ith_freq]
            self.st[name][_id] = abs(f * D / U)

    def _get_cdcl_values(self):
        for case in self.cases:
            name = case.params['io_method']
            re = case.params['re']
            nx = case.params['nx']
            _id = 're_' + str(re) + '_nx_' + str(nx)
            if not self.cl.get(name):
                self.cl[name] = {}
                self.cd[name] = {}
            data = case.data
            t = data['t']
            cl = data['cl']
            cd = data['cd']
            self.cl[name][_id] = self._evaluate_cl(cl)
            self.cd[name][_id] = np.average(cd[-100:])


    def _evaluate_cl(self, cl):
        clnew = np.convolve(cl, np.ones((5,))/5, mode='valid')
        return 0.5 * (max(clnew) - min(clnew))

    def _table_compare_cd_cl_st_all(self):
        column_names = ['Method', r'$c_d$', r'$c_l$', r'St']

        rows = []
        _id = 're_' + str(200) + '_nx_' + str(30)

        for name in IO_NAMES:
            iname = name
            if name == "mod_donothing":
                iname = "New do-nothing"
            if name == "donothing":
                iname = "do-nothing"
            if name == 'mirror':
                continue
            row = [iname.title()]
            row.append(r'$%.3f \pm 0.05$'%self.cd[name][_id])
            row.append(r'$\pm %.3f$'%self.cl[name][_id])
            row.append(r'$\pm %.3f$'%self.st[name][_id])
            rows.append(row)

        row = [r'\citet{guerrero2009numerical}']
        row.append(r'$1.409 \pm 0.048 $')
        row.append(r'$ \pm 0.725$')
        row.append(r'-')
        rows.append(row)

        row = [r'\citet{open_bc:tafuni:cmame:2018}']
        row.append(r'$1.46$')
        row.append(r'$ \pm 0.693$')
        row.append(r'$ 0.206$')
        rows.append(row)

        row = [r'\citet{MARRONE2013456}']
        row.append(r'$1.38 \pm 0.05$')
        row.append(r'$ \pm 0.680$')
        row.append(r'$ 0.200$')
        rows.append(row)

        make_table(
            column_names, rows, self.output_path(self.get_name() +
                '_cd_cl_st.tex'),
            sort_cols=[0, -1], column_format='lrrr'
        )

    def _plot_particles(self):
        from pysph.solver.utils import get_files, load

        io_method = [x for x in self.case_info if
                     (self.case_info[x]['re']==200 and self.case_info[x]['nx']==30)]
        times = [50, 150]
        for name in io_method:
            # if  == 20:
            #     continue
            fname = 'flow_past_cylinder_2d'
            files = get_files(self.input_path(name), fname)
            logfile = os.path.join(self.input_path(name), fname + '.log')
            to_plot = get_files_at_given_times_from_log(files, times, logfile)
            io_name = name.split('_re')[0]
            print(to_plot)
            if io_name not in self.plotdata:
                self.plotdata[io_name] = {}
                self.plotdata[io_name]['x'] = {}
                self.plotdata[io_name]['y'] = {}
            for scalar in ('p', 'vmag'):
                if scalar not in self.plotdata[io_name]:
                    self.plotdata[io_name][scalar] = {}
                for i, f in enumerate(to_plot):
                    data = load(f)
                    pa = data['arrays']['fluid']
                    if scalar == 'vmag':
                        u = self.normalize_u(np.sqrt(pa.u*pa.u+pa.v*pa.v))
                        self.plotdata[io_name][scalar][i] = u
                    else:
                        p = self.normalize_p(pa.p)
                        self.plotdata[io_name][scalar][i] = p
                    x0 = pa.x.copy()
                    self.plotdata[io_name]['x'][i] = x0
                    y0 = pa.y.copy()
                    self.plotdata[io_name]['y'][i] = y0

        self._plot_('p')
        self._plot_('vmag')

    def _plot_(self, var):
        label = var
        if var == 'vmag':
            label = 'u'
        import matplotlib.pyplot as plt
        times = [50, 150]
        io_name = IO_NAMES
        iname = INAMES
        _IO_NAMES = ['donothing', 'hybrid', 'mod_donothing', 'characteristic']
        _INAMES = ['do-nothing', 'hybrid', 'New do-nothing', 'characteristic']
        for j, time in enumerate(times):
            if j == 0 :
                fig = plt.figure(figsize=(8, 12))
                grid = plt.GridSpec(3, 4, hspace=0.3, wspace=0.3)
                ax = []
                ax.append(fig.add_subplot(grid[0, 0:2]))
                ax[-1].xaxis.set_tick_params(labelbottom=False)
                ax.append(fig.add_subplot(grid[0, 2:4]))
                ax[-1].xaxis.set_tick_params(labelbottom=False)
                ax[-1].yaxis.set_tick_params(labelbottom=False)
                ax.append(fig.add_subplot(grid[1, 0:2]))
                ax.append(fig.add_subplot(grid[1, 2:4]))
                ax[-1].yaxis.set_tick_params(labelbottom=False)
                ax.append(fig.add_subplot(grid[2, 1:3]))
            else:
                fig = plt.figure(figsize=(8, 8))
                grid = plt.GridSpec(2, 4, hspace=0.3, wspace=0.3)
                ax = []
                io_name = _IO_NAMES
                iname = _INAMES
                ax.append(fig.add_subplot(grid[0, 0:2]))
                ax[-1].xaxis.set_tick_params(labelbottom=False)
                ax.append(fig.add_subplot(grid[0, 2:4]))
                ax[-1].xaxis.set_tick_params(labelbottom=False)
                ax[-1].yaxis.set_tick_params(labelbottom=False)
                ax.append(fig.add_subplot(grid[1, 0:2]))
                ax.append(fig.add_subplot(grid[1, 2:4]))
                ax[-1].yaxis.set_tick_params(labelbottom=False)
            for i, axes in enumerate(ax):
                axes.axis('equal')
                x = self.plotdata[io_name[i]]['x'][j]
                y = self.plotdata[io_name[i]]['y'][j]
                p = self.plotdata[io_name[i]][var][j]
                if io_name[i] == 'hybrid' and j == 1 and var == 'p':
                    img = axes.scatter(x, y, c=p, s=0.1, cmap='seismic', vmin=-1, vmax=3.0)
                elif io_name[i] == 'donothing' and j == 1 and var == 'p':
                    img = axes.scatter(x, y, c=p, s=0.1, cmap='seismic', vmin=1, vmax=2.5)
                elif io_name[i] == 'characteristic' and j == 1 and var == 'p':
                    img = axes.scatter(x, y, c=p, s=0.1, cmap='seismic', vmin=-1, vmax=1.0)
                elif io_name[i] == 'mod_donothing' and j == 1 and var == 'p':
                    img = axes.scatter(x, y, c=p, s=0.1, cmap='seismic', vmin=1.5, vmax=4.5)
                elif var == 'vmag':
                    img = axes.scatter(x, y, c=p, s=0.1, cmap='seismic', vmin=0, vmax=2.0)
                else:
                    img = axes.scatter(x, y, c=p, s=0.1, cmap='seismic')
                axes.set_title(iname[i].title())
                plt.colorbar(img, ax=axes, pad=0, label=label)

            plt.savefig(self.output_path(var + str(j) + '.png'),
                        dpi=300)
            plt.close()


class TestCase(Problem):
    def normalize_p(self, p):
        # normalize by dynamic pressure
        return p / (0.5 * self.rho * self.umax**2)

    def normalize_u(self, u):
        # normalize by inlet velocity
        return u / self.umax

    def normalize_ux(self, ux):
        # normalize by inlet velocity / length of tunnel
        return ux / (self.umax / self.lt)

    def normalize_px(self, px):
        # normalize by dynamic pressure / length of tunnel
        return px / (0.5 * self.rho * self.umax**2 / self.lt)

    def _readdata(self):
        for case in self.cases:
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            self.data[name] = case.data

    def _get_l2_norms(self, vars=['p', 'u', 'v'], num=None):

        column_names = ['Methods']
        format = 'l'
        for var in vars:
            column_names.append(r'$e(%s^{*})$'%var)
            format += 'r'

        rows = []

        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                continue
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            row = []
            row.append(name.title())
            for var in vars:
                if num is None:
                    if var == 'p':
                        val = self.normalize_p(self.data['long'][var])
                    else:
                        val = self.normalize_u(self.data['long'][var])
                else:
                    if var == 'p':
                        val = self.normalize_p(self.data['long'][var][num])
                    else:
                        val = self.normalize_u(self.data['long'][var][num])
                val_e = savgol_filter(val, 21, 5)

                if num is None:
                    if var == 'p':
                        _val = self.normalize_p(self.data[name][var])
                    else:
                        _val = self.normalize_u(self.data[name][var])
                else:
                    if var == 'p':
                        _val = self.normalize_p(self.data[name][var][num])
                    else:
                        _val = self.normalize_u(self.data[name][var][num])
                _val_f = savgol_filter(_val, 21, 5)
                error = (sum((val_e - _val_f)**2) / sum(val_e**2))**(0.5)
                row.append('%.3f'%error)
            rows.append(row)

        make_table(
            column_names, rows, self.output_path(self.get_name() +
                '_l2.tex'),
            sort_cols=[0, -1], column_format=format
        )

    def plot_u_all(self, var, num=None):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            linestyle = next(linestyles)
            color = next(colors)
            if num is None:
                u = self.normalize_u(self.data[name][var])
            else:
                u = self.normalize_u(self.data[name][var][num])
            uf = savgol_filter(u, 21, 5)
            plt.plot(
                self.data[name]['t'],
                uf, linestyle=linestyle,
                color=color,
                label=name.title(),
                markevery= 7 + i
            )
        plt.xlabel(r't (sec)')
        plt.ylabel(r'$u^*$')
        plt.grid()
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('u.png'), dpi=300)
        plt.close()

    def plot_v_all(self, var, num=None):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            linestyle = next(linestyles)
            color = next(colors)
            if num is None:
                v = self.normalize_u(self.data[name][var])
            else:
                v = self.normalize_u(self.data[name][var][num])
            vf = savgol_filter(v, 21, 5)
            plt.plot(
                self.data[name]['t'],
                vf, linestyle=linestyle,
                color=color,
                label=name.title(),
                markevery=7 + i
            )
        plt.xlabel(r't (sec)')
        plt.ylabel(r'$v^*$')
        plt.grid()
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('v.png'), dpi=300)
        plt.close()

    def plot_p_all(self, var, num=None):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            linestyle = next(linestyles)
            color = next(colors)
            if num is None:
                p = self.normalize_p(self.data[name][var])
            else:
                p = self.normalize_p(self.data[name][var][num])
            pf = savgol_filter(p, 21, 5)
            plt.plot(
                self.data[name]['t'],
                pf, linestyle=linestyle,
                color=color,
                label=name.title(),
                markevery=7 + i

            )
        plt.xlabel(r't (sec)')
        plt.ylabel(r'$p^*$')
        plt.grid()
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('p.png'), dpi=300)
        plt.close()

    def plot_px_all(self, var, num=None):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            linestyle = next(linestyles)
            color = next(colors)
            if num is None:
                px = self.normalize_p(self.data[name][var])
            else:
                px = self.normalize_p(self.data[name][var][num])
            pxf = savgol_filter(px, 21, 5)
            plt.plot(
                self.data[name]['t'],
                pxf, linestyle=linestyle,
                color=color,
                label=name.title(),
                markevery=7 + i
            )
        plt.xlabel(r't (sec)')
        plt.ylabel(r'$\frac{\partial p^*}{\partial x}$')
        plt.grid()
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('px.png'), dpi=300)
        plt.close()

    def plot_ux_all(self, var, num=None):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            linestyle = next(linestyles)
            color = next(colors)
            if num is None:
                ux = self.normalize_u(self.data[name][var])
            else:
                ux = self.normalize_u(self.data[name][var][num])
            uxf = savgol_filter(ux, 21, 5)
            plt.plot(
                self.data[name]['t'],
                uxf, linestyle=linestyle,
                color=color,
                label=name.title(),
                markevery=7 + i
            )
        plt.xlabel(r't (sec)')
        plt.ylabel(r'$\frac{\partial u^*}{\partial x}$')
        plt.grid()
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('ux.png'), dpi=300)
        plt.close()

    def plot_vx_all(self, var, num=None):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            linestyle = next(linestyles)
            color = next(colors)
            if num is None:
                vx = self.normalize_u(self.data[name][var])
            else:
                vx = self.normalize_u(self.data[name][var][num])
            vxf = savgol_filter(vx, 21, 5)
            plt.plot(
                self.data[name]['t'],
                vxf, linestyle=linestyle,
                color=color,
                label=name.title(),
                markevery=7 + i
            )
        plt.xlabel(r't (sec)')
        plt.ylabel(r'$\frac{\partial v^*}{\partial x}$')
        plt.grid()
        plt.legend()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('vx.png'), dpi=300)
        plt.close()

    def plot_u_in_outlet(self, var):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            plt.plot(
                self.data[name]['x1'],
                self.normalize_u(self.data[name][var]),
                linestyle=next(linestyles), color=next(colors),
                label=name.title(),markevery=7 + i
            )
        plt.xlabel(r'x (m)')
        plt.ylabel(r'$u^*$')
        plt.legend()
        plt.xlim([1.5, 2.5])
        ylim = plt.ylim()
        plt.plot([2.0, 2.0], [ylim[0], ylim[1]], 'r--')
        plt.grid()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('u_in_outlet.pdf'))
        plt.close()

    def plot_p_in_outlet(self, var):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            plt.plot(
                self.data[name]['x1'],
                self.normalize_p(self.data[name][var]),
                linestyle=next(linestyles), color=next(colors),
                label=name.title(), markevery=7 + i
            )
        plt.xlabel(r'x (m)')
        plt.ylabel(r'$p^*$')
        plt.legend()
        plt.xlim([1.5, 2.5])
        ylim = plt.ylim()
        plt.plot([2.0, 2.0], [ylim[0], ylim[1]], 'r--')
        plt.grid()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('p_in_outlet.pdf'))
        plt.close()

    def plot_ux_in_outlet(self, var):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            plt.plot(
                self.data[name]['x1'],
                self.normalize_ux(self.data[name][var]),
                linestyle=next(linestyles),  color=next(colors),
                label=name.title(), markevery=7 + i
            )
        plt.xlabel(r'x ( (m)m)')
        plt.ylabel(r'${\frac{du}{dx}}^{*}$')
        plt.legend()
        plt.xlim([1.5, 2.5])
        ylim = plt.ylim()
        plt.plot([2.0, 2.0], [ylim[0], ylim[1]], 'r--')
        plt.grid()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('ux_in_outlet.pdf'))
        plt.close()

    def plot_px_in_outlet(self, var):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        plt.figure(figsize=(4, 4))
        for i, case in enumerate(self.cases):
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            if case.name == 'mod_donothing':
                name = 'new do-nothing'
            if case.name == 'donothing':
                name = 'do-nothing'
            plt.plot(
                self.data[name]['x1'],
                self.normalize_px(self.data[name][var]),
                linestyle=next(linestyles), color=next(colors),
                label=name.title(), markevery=7 + i
            )
        plt.xlabel(r'x (m)')
        plt.ylabel(r'${\frac{dp}{dx}}^{*}$')
        plt.legend()
        plt.xlim([1.5, 2.5])
        ylim = plt.ylim()
        plt.plot([2.0, 2.0], [ylim[0], ylim[1]], 'r--')
        plt.grid()
        plt.tight_layout(pad=0)
        plt.savefig(self.output_path('px_in_outlet.pdf'))
        plt.close()


class Pulse2d(TestCase):
    def get_name(self):
        return 'pulse2d'

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 40
        self.wt = 2.0
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/wave_2d.py --tf 3.0 --pfreq 4' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt
            )
            for method in _case_info
        }

        self.case_info['exact'] = dict(
                io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt
        )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items() if name not in
            ['exact']
        ]

        cmd_exact = 'python code/wave_2d.py --tf 3.0 --pfreq 4' +\
            backend
        self.cases.append(
            Simulation(
                    get_path('exact'), cmd_exact,
                    job_info=dict(n_core=4, n_thread=4), cache_nnps=None,
                    **scheme_opts(self.case_info['exact']))
            )

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.plot_p_in_outlet('pt')
        self.plot_u_in_outlet('ut')
        self.plot_px_in_outlet('ptx')
        self.plot_ux_in_outlet('utx')
        self.plot_p_all('p', 2)
        self.plot_u_all('u', 2)
        self._get_l2_norms(vars=['p', 'u'], num=2)


class SingleVortex(TestCase):
    def get_name(self):
        return 'vortex'

    def get_re(self):
        return 0.0

    def _get_cmd(self):
        return 'python code/vortex_generation.py --tf 4 --pfreq 4' +\
            backend

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 20
        self.wt = 2.0
        self.data = {}
        re = self.get_re()

        get_path = self.input_path
        cmd = self._get_cmd()

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt,
                re=re
            )
            for method in _case_info
        }

        self.case_info['exact'] = dict(
            io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt,
            re=re
        )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items() if name not in
            ['exact']
        ]

        cmd_exact = self._get_cmd()

        self.cases.append(
            Simulation(
                get_path('exact'), cmd_exact,
                job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(self.case_info['exact'])
            )
        )

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.plot_p_all('p', 5)
        self.plot_u_all('u', 5)
        self.plot_v_all('v', 5)
        self._get_l2_norms(vars=['p', 'u', 'v'], num=5)


class ViscousVortexRe100(SingleVortex):
    def get_name(self):
        return 'vortex_re100'

    def get_re(self):
        return 100.0


class ViscousVortexRe10000(SingleVortex):
    def get_name(self):
        return 'vortex_re10000'

    def get_re(self):
        return 10000.0


class Pulse1d(TestCase):
    def get_name(self):
        return 'pulse1d'

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 20
        self.wt = 1.0
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/wave_1d.py --tf 3.0 --pfreq 4' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt
            )
            for method in _case_info
        }

        self.case_info['exact'] = dict(
                io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt
        )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items() if name not in
            ['exact']
        ]

        cmd_exact = 'python code/wave_1d.py --tf 3.0 --pfreq 4' +\
            backend
        self.cases.append(
            Simulation(
                get_path('exact'), cmd_exact,
                job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(self.case_info['exact'])
            )
        )

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.plot_p_all('p')
        self.plot_u_all('u')
        self._get_l2_norms(vars=['p', 'u'])


class Pulse1dViscous(TestCase):
    def get_name(self):
        return 'pulse1d_visc'

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 20
        self.wt = 1.0
        self.nu = 0.01
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/wave_1d.py --tf 3.0 --pfreq 4' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt,
                nu=self.nu
            )
            for method in _case_info
        }

        self.case_info['exact'] = dict(
                io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt,
                nu=self.nu
        )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items() if name not in
            ['exact']
        ]

        cmd_exact = 'python code/wave_1d.py --tf 3.0 --pfreq 4' +\
            backend
        self.cases.append(
            Simulation(
                get_path('exact'), cmd_exact,
                job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(self.case_info['exact'])
            )
        )

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.plot_p_all('p')
        self.plot_u_all('u')
        self._get_l2_norms(vars=['p', 'u'])


class Ramp(TestCase):
    def get_name(self):
        return 'ramp'

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 20
        self.wt = 1.0
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/ramp.py --tf 3.0 --pfreq 4' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt
            )
            for method in _case_info
        }

        self.case_info['exact'] = dict(
                io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt
        )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items() if name not in
            ['exact']
        ]

        cmd_exact = 'python code/ramp.py --tf 3.0 --pfreq 4' +\
            backend
        self.cases.append(
            Simulation(
                get_path('exact'), cmd_exact,
                job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(self.case_info['exact'])
            )
        )

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.plot_p_all('p', 2)
        self.plot_u_all('u', 2)
        self._get_l2_norms(vars=['p', 'u'], num=2)


class PressurePulse(Problem):
    def get_name(self):
        return 'pressure_pulse'

    def normalize_p(self, p):
        # normalize by dynamic pressure
        return p / (0.5 * self.rho * self.umax**2)

    def normalize_u(self, u):
        # normalize by inlet velocity
        return u / self.umax

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 1.0
        self.nx = 40
        self.wt = 1.0
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/pressure_pulse.py --tf .17' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt
            )
            for method in _case_info
        }

        self.case_info['exact'] = dict(
                io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt
        )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items() if name not in
            ['exact']
        ]

        cmd_exact = 'python code/pressure_pulse.py --tf .17' +\
            backend
        self.cases.append(
            Simulation(
                get_path('exact'), cmd_exact,
                job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(self.case_info['exact'])
            )
        )

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def _readdata(self):
        for case in self.cases:
            name = case.params['io_method']
            if case.name == 'exact':
                name = 'long'
            self.data[name] = case.data

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.plot_p_all()

    def plot_p_all(self):
        import matplotlib.pyplot as plt

        linestyles = LINESTYLE
        colors = COLOR
        fig, ax = plt.subplots(
                5, 1, sharex='all', figsize=(8, 8))
        times = [0, .0336, .0548, .1016, .1695]
        for i, time in enumerate(times):
            for j, case in enumerate(self.cases):
                name = case.params['io_method']
                if case.name == 'exact':
                    name = 'long'
                linestyle = next(linestyles)
                color = next(colors)
                t = self.data[name]['t']
                idx =  np.where(abs(t - time) < 0.01)[0][0]

                x = self.data[name]['x']
                ax[i].plot(
                    x,
                    self.data[name]['pt'][idx], linestyle=linestyle,
                    color=color,
                    label=name.upper(), markevery=7 + j
                )
                ax[4].set_xlabel('x')
                ax[i].set_ylabel('p')
                ax[i].set_title('t = %s sec'%str(t[idx]), loc='right', pad=0)
                ax[i].grid(True)

        leg = list(self.data.keys())
        fig.legend(leg , ncol=len(leg), fontsize= 'x-small', loc='upper center')
        fig.savefig(self.output_path('p.png'), dpi=300)
        plt.close()


class BackwardFacingStep(Problem):
    def get_name(self):
        return 'backward_facing_step'

    def normalize_p(self, p):
        # normalize by dynamic pressure
        return p / (0.5 * self.rho * self.umax**2)

    def normalize_u(self, u):
        # normalize by inlet velocity
        return u / self.umax

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.nx = 40
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/backward_facing_step_2d.py --tf 1.2 --pfreq 500' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            # 'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            # 'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}': dict(
                **_case_info[method]
            )
            for method in _case_info
        }

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=5, n_thread=64),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def _readdata(self):
        for case in self.cases:
            name = case.params['io_method']
            self.data[name] = case.data

    def _read_exact(self):
        dirname = os.path.dirname(__file__)
        fractions = [2.55, 3.57, 4.80, 7.14]
        exact = {}

        for frac in fractions:
            filename = os.path.join(dirname, 'manuscript',
                                    'data', 'bfs', '%.2f.csv' % frac)
            u, y = np.loadtxt(filename, dtype=float,
                              delimiter=',', unpack=True)
            exact[frac] = {}
            exact[frac]['u'] = u
            exact[frac]['y'] = y

        return exact

    def run(self):
        self.make_output_dir()
        self._readdata()
        self._plot_vel()
        self._make_table()

    def _plot_vel(self):
        import matplotlib.pyplot as plt
        fractions = [2.55, 3.57, 4.80, 7.14]
        exact = self._read_exact()

        _LINESTYLE = cycle([
            (0, (1, 1)),
            (0, (5, 1, 1, 1)),

            (0, (5, 1, 1, 1, 1, 1)),
            (0, (5, 1, 1, 1, 1, 1, 1, 1)),
            ])
        _COLOR = cycle(['r', 'g', 'c'])
        fig, ax = plt.subplots(
                1, 4, sharey='all', figsize=(8, 4))
        _IO_NAMES = ['donothing', 'hybrid', 'mod_donothing']
        _INAMES = ['do-nothing', 'hybrid', 'New do-nothing']
        for j, name in enumerate(_IO_NAMES):
            data = self.data[name]
            print(data['rl'], name)
            x = data['x']
            linestyle = next(_LINESTYLE)
            color = next(_COLOR)
            for i, xi in enumerate(x):
                y = data['y1']
                u = data['u%d' % i]
                if i == 0:
                    ax[i].plot(u, y, linestyle=linestyle,
                               color=color, label=_INAMES[j].title())
                else:
                    ax[i].plot(u, y, linestyle=linestyle, color=color )

        for i, xi in enumerate(fractions):
            y = exact[xi]['y']/1000
            u = exact[xi]['u']
            if i == 0:
                ax[i].plot(u, y, 'ok', label='Armaly et. al', fillstyle='none')
                ax[i].set_ylabel('y(mm)')
            else:
                ax[i].plot(u, y, 'ok', fillstyle='none')
            ax[i].set_title('x/s=%.2f'%xi)
            ax[i].grid()
            ax[i].set_xlabel('u(m/s)')

        fig.legend(ncol=len(_IO_NAMES)+1, fontsize= 'x-small', loc='upper center')
        fig.savefig(self.output_path('u.png'), dpi=300)
        plt.close()

    def _make_table(self):
        column_names = ['Method', r'$x_{rl}/h$']
        _IO_NAMES = ['donothing', 'hybrid', 'mod_donothing']
        _INAMES = ['do-nothing', 'hybrid', 'New do-nothing']

        rows = []
        for j, name in enumerate(_IO_NAMES):
            row = []
            data = self.data[name]
            row.append(_INAMES[j].title())
            row.append('%.3f'%(data['rl']/0.0049))
            rows.append(row)

        make_table(
            column_names, rows, self.output_path(self.get_name() +
                '_rl.tex'),
            sort_cols=[0, -1], column_format='lr'
        )


class Pulse2dEDACTest(TestCase):
    def get_name(self):
        return 'pulse2d_edac_test'

    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 40
        self.wt = 2.0
        self.edac_alpha = [0.1, 0.2, 0.5, 1.0]
        self.data = {}

        get_path = self.input_path
        cmd = 'python code/wave_2d.py --tf 3.0 --pfreq 4' +\
            backend

        _case_info = {
            'donothing': dict(io_method='donothing'),
            'mirror': dict(io_method='mirror'),
            'hybrid': dict(io_method='hybrid'),
            'characteristic': dict(io_method='characteristic'),
            'mod_donothing': dict(io_method='mod_donothing'),
        }

        self.case_info = {
            f'{method}_{edac_alpha}': dict(
                **_case_info[method], lt=self.lt, nx=self.nx, wt=self.wt,
                edac_alpha=edac_alpha
            )
            for method, edac_alpha in product(_case_info, self.edac_alpha)
        }

        for edac_alpha in self.edac_alpha:
            name = f'exact_{edac_alpha}'
            self.case_info[name] = dict(
                io_method='donothing', lt=40.0, nx=self.nx, wt=self.wt,
                edac_alpha=edac_alpha
            )

        self.cases = [
            Simulation(
                get_path(name), cmd, job_info=dict(n_core=4, n_thread=4),
                cache_nnps=None, **scheme_opts(Kwargs)
            ) for name, Kwargs in self.case_info.items()
        ]

        for case in self.cases:
            self.case_info[case.name]['case'] = case

    def run(self):
        self.make_output_dir()
        self._readdata()
        self.__get_l2_norms_p()
        self.__get_l2_norms_u()

    def _readdata(self):
        for case in self.cases:
            name = case.name
            self.data[name] = case.data

    def __get_l2_norms_p(self):

        column_names = ['Methods']
        format = 'l'
        for alpha in self.edac_alpha:
            column_names.append(r'$\alpha = %.1f$'%alpha)
            format += 'r'

        rows = []

        for i, case in enumerate(IO_NAMES):
            base_name = INAMES[i]
            row = []
            row.append(base_name.title())
            for alpha in self.edac_alpha:
                val = self.normalize_p(self.data[f'exact_{alpha}']['p'][2])
                val_e = savgol_filter(val, 21, 5)

                case_name = f'{case}_{alpha}'
                _val = self.normalize_p(self.data[case_name]['p'][2])
                _val_f = savgol_filter(_val, 21, 5)
                error = (sum((val_e - _val_f)**2) / sum(val_e**2))**(0.5)
                row.append('%.3f'%error)
            rows.append(row)

        make_table(
            column_names, rows, self.output_path(self.get_name() +
                '_l2_p.tex'),
            sort_cols=[0, -1], column_format=format
        )

    def __get_l2_norms_u(self):

        column_names = ['Methods']
        format = 'l'
        for alpha in self.edac_alpha:
            column_names.append(r'$\alpha = %.1f$'%alpha)
            format += 'r'

        rows = []

        for i, case in enumerate(IO_NAMES):
            base_name = INAMES[i]
            row = []
            row.append(base_name.title())
            for alpha in self.edac_alpha:
                val = self.normalize_p(self.data[f'exact_{alpha}']['u'][2])
                val_e = savgol_filter(val, 21, 5)

                case_name = f'{case}_{alpha}'
                _val = self.normalize_p(self.data[case_name]['u'][2])
                _val_f = savgol_filter(_val, 21, 5)
                error = (sum((val_e - _val_f)**2) / sum(val_e**2))**(0.5)
                row.append('%.3f'%error)
            rows.append(row)

        make_table(
            column_names, rows, self.output_path(self.get_name() +
                '_l2_u.tex'),
            sort_cols=[0, -1], column_format=format
        )


if __name__ == '__main__':
    PROBLEMS = [Pulse1d, Pulse1dViscous, Pulse2d, SingleVortex,
                FlowPastCylinder, Ramp, PressurePulse, BackwardFacingStep,
                ViscousVortexRe100, ViscousVortexRe10000, Pulse2dEDACTest]

    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()
