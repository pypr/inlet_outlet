\section{Results and discussion}
\label{sec:results}

In this section, we compare the different methods for the outlet boundary
condition with a variety of test cases. Each of these cases only takes a small
amount of computational effort and highlights specific issues. The new
problems are all two-dimensional and this makes them relatively easy to
implement. They include a one dimensional pulse (in a two-dimensional domain),
a two-dimensional pulse, a two-dimensional vortex, and a ramp inlet condition
in order to test the typical conditions that outlets encounter. In order to
obtain a solution representing an infinite domain for comparison, we simulate
the flow in a very long domain. The properties of the fluid are measured at a
probe placed inside the domain at a distance $d$ from the inlet. The length of
the domain, $L$ is chosen to be $d + c_st$, and $t$ is the simulation time. We
treat the fluid as inviscid and use a particle spacing of $\Delta x = 0.1$
unless stated otherwise in all our testcases. We use the results in the long
domain as a reference and use this to compute the $L_{2}$ norm of the errors
in the various properties using
\begin{equation}
  \label{eq:l2norm}
  e(f) = \left( \frac{\sum_n (f^n - f^n_l)^{2}}{\sum_n (f^{n}_l)^{2}} \right)^{1/2},
\end{equation}
where $n$ represents the timestep, $f^n$ is the property of interest at a
particular timestep and $f^n_l$ is the corresponding property in the long
domain. Once these test cases are simulated we demonstrate the best of these
methods for an impulsively started flow past a circular cylinder at different
Reynolds numbers and a backward-facing step.

\subsection{1D Pressure bump}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.7]{figures/pressure_pulse/p.png}
  \caption{Pressure plot at various times for the different methods.  The
    solid line denotes the solution with the long domain.}
  \label{fig:pressurepulse}
\end{figure}
This test case was proposed by \citet{Lastiwka2009:nonrefbc}. In this testcase,
the fluid domain is initialized with a pressure variation given by
\begin{equation}
	p(x) = 1.0 - 0.2 e^{\frac{-(x-0.5)^2}{0.001}}.
	\label{eq:pressurebump}
\end{equation}
The pressure at inlet and outlet is initialized with $p=1.0$. Velocity of the
domain including inlet and outlet remain constant (=1m/s) for all times. The
domain length is $1m$ and the pressure bump is at $x=0.5m$. We use the
artificial viscosity parameter, $\alpha=0.1$ as mentioned in
\cite{Lastiwka2009:nonrefbc}. We simulated the testcase for all the types of
outlet boundaries described in the section \ref{sec:bc}. In case of the MOC
for all the test cases $u_{ref}, p_{ref} $ and $\rho_{ref}$ is taken as $1.0
m/s$, $1.0\ Pa$ and $1000 kg/m^3$ respectively. In
Fig.~\ref{fig:pressurepulse}, we compare the pressure along the centerline of
the domain at different times for all the methods. It can be seen that
mirroring technique results in a significant drop in pressure towards the end.
The modified do-nothing increases the pressure in the domain by a small
amount. All other cases, match well with the MOC and with the long domain.


\subsection{2D pulse}
\label {subsec:2dpulse}

This benchmark tests the non-reflectivity for a two-dimensional disturbance. A
2D domain is considered, consisting of fluid with domain length, $L=2 m$ and
width, $W=2 m$. The probe is placed at $d=1.7m$ from the inlet. The fluid
region is constrained by inviscid walls on both sides. The inflow is taken
from the left and outlet is kept at the right of the fluid. The inlet, wall,
and outlet are initialized with $6$ layers of particles. In order to introduce
a 2D variation, the $u$ velocity is made a function of $y$, given by
\begin{equation}
  u(x,y,t) =  \left\{ \begin{array} { l l } {1.0 + 0.5 \cos \left(
        \frac{ \pi y}{12} \right)
      e^{\frac{{(t-1)}^{2}}{\delta} }} & {
			1.0 < t < 1.1} \\ {1.0
      } & { \text{elsewhere} }
  \end{array} \right..
  \label{eq:velfunc2}
\end{equation}
We normalize $p$ and $u$ measured at the probe such that $u^*=u/u_{ref}$ and
$p^*=\frac{2p}{\rho u_{ref}^2}$ respectively. Fig.~\ref{fig:pulse} shows the
plot of $u^*$ and $p^*$ versus time for the different outlets and
Table~\ref{table:pulse2d_l2} shows $L_2$ errors in the pressure and velocity
for the different outlet implementations.
\begin{figure}[h!]
	\centering
  \includegraphics[width=0.45\textwidth]{figures/pulse2d/p.png}
  \includegraphics[width=0.45\textwidth]{figures/pulse2d/u.png}
	\caption{Normalized pressure (left) and velocity (right) plots at
    $x=1.7m$ with time for 2D varying inlet.}
	\label{fig:pulse}
\end{figure}

The pressure variation with the MOC and hybrid methods are very close to the
results for a long domain compared to mirroring and do-nothing outlet. It can
be seen that the mirroring technique generates a lot of reflections into the
fluid as compared to do-nothing and MOC. In case of the do-nothing a
significant increase in pressure can be seen just after the wave passes
through the outlet (at around $1.25s$).

\begin{table}[h!]
\centering
\input{figures/pulse2d/pulse2d_l2.tex}
\caption{$L_2$ error in the $p^*$ and $u^*$ measured at the probe for the 2D pulse
problem.}
\label{table:pulse2d_l2}
\end{table}
Looking at the variation of the velocity we can see that both the modified
do-nothing and the new hybrid method show a close match to the results for a
long domain. After $2s$, the MOC method differs from the long-domain results
due to the spatial variations arriving near the outlet. The modified
do-nothing method is clearly better than the standard do-nothing scheme. These
conclusions are also borne out by the values of the $L_2$ norm as seen in
Table~\ref{table:pulse2d_l2}. The proposed hybrid method has the least errors.
\begin{figure}[h!]
	\centering
  \includegraphics[width=0.45\textwidth]{figures/pulse2d/p_in_outlet.pdf}
  \includegraphics[width=0.45\textwidth]{figures/pulse2d/u_in_outlet.pdf}
  \caption{Normalized pressure (left) and velocity (right) along the $y=0$ line for
    the 2D pulse problem. Left of the dashed red line is fluid and right is
    outlet region.}
	\label{fig:extrapolation}
\end{figure}

\begin{figure}[h!]
	\centering
  \includegraphics[width=0.45\textwidth]{figures/pulse2d/px_in_outlet.pdf}
  \includegraphics[width=0.45\textwidth]{figures/pulse2d/ux_in_outlet.pdf}
  \caption{Normalized pressure (left) and velocity gradients (right) along the
    $y=0$ line for the 2D pulse problem. Left of the dashed red line is fluid
    and right is outlet region.}
	\label{fig:extrapolationgrad}
\end{figure}
In order to show the nature of the property variation across the fluid outlet
interface due to extrapolation, we interpolated pressure, velocity and their
gradients on a $y=0$ line as shown in Figure \ref{fig:extrapolation} and
\ref{fig:extrapolationgrad}. It can be seen that, in case of the mirroring
technique that the gradient of the property is zero at the interface. The
property is mirrored about the domain boundary however both hybrid and
do-nothing retain the history of the particle such that velocity and pressure
in the outlet do not affect the upstream flow. On looking at the gradient
along $x$ of the property for all the methods in
Fig.\ref{fig:extrapolationgrad}, we find that the mirroring technique impose
natural boundary conditions on fluid particles near the outlet i.e $\partial u/
\partial x = 0$, and $\partial p /\partial x =0$. In case of do-nothing and
modified do-nothing, the velocity and pressure profiles matched the long
domain but gradient changes significantly. However, the method of
characteristics and hybrid maintains the flow gradients along with the flow
variables as they are. In the context of the SPH, the latter seems to be very
important.

As discussed in section~\ref{sec:sph}, the EDAC method involves a parameter
called $\alpha$ which increases the pressure damping. We explore varying the
parameter $\alpha$ and study the error in $p^*$ for the different schemes in
Table~\ref{table:pulse2d_edac_p}. It can be observed that as $\alpha$
increases the pressure oscillations are reduced and therefore the errors
reduce for all the schemes. However, the greatest reduction is for the
original do-nothing and mirror methods. The others are not significantly
affected. This suggests that the hybrid method and modified do-nothing are
robust techniques.

\begin{table}[h!]
\centering
\input{figures/pulse2d_edac_test/pulse2d_edac_test_l2_p.tex}
\caption{$L_2$ error in $p^*$ measured at the probe for the 2D pulse as the
  EDAC parameter $\alpha$.}
\label{table:pulse2d_edac_p}
\end{table}

\subsection{1D ramp}
\label{subsec:ramp}

In this test case, we impose a ramp velocity on the inlet particles such that
$u =0 m /s$ at $t = 0 s$ and $u = 1 m/s$ at $t = 1 s$. After time $t = 1s$,
the velocity is fixed at $1 m/s$. The size of domain, boundary condition and
initialization are same as in the case of the 2D pulse. We simulate the test
case for each method and compare it with results for a long domain.
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.45\textwidth]{figures/ramp/p.png}
  \includegraphics[width=0.45\textwidth]{figures/ramp/u.png}
  \caption{Normalized pressure (left) and velocity (right) plots at
  $x=1.7m$ with time for ramp inlet.}
  \label{fig:ramp_pu}
\end{figure}
In the Figure \ref{fig:ramp_pu}, we have plotted the $p^*, u^*$ for the ramp
and the $L_2$ errors are shown in Table~\ref{table:ramp_l2}. In case of the
pressure, the hybrid, mirror, and modified do-nothing methods work well. The
standard do-nothing method generates a significantly high pressure as the
initial particles at the outlet do not move and thereby cause an increase in
pressure. In case of the MOC, there is no specific method to determine the
reference values for $u, p$ at the initial stage and this seems to cause the
problems. Similar issues are seen in the case of the velocity for the MOC. As
seen in Table~\ref{table:ramp_l2}, the hybrid method has the least errors for
both pressure and velocity.

\begin{table}[h!]
\centering
\input{figures/ramp/ramp_l2.tex}
\caption{$L_2$ error in the $p^*$ and $u^*$ measured at the probe for the ramp
  velocity problem.}
\label{table:ramp_l2}
\end{table}

\subsection{2D vortex}
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.45\textwidth]{figures/vortex/p.png}
  \includegraphics[width=0.45\textwidth]{figures/vortex/u.png}
  \includegraphics[width=0.45\textwidth]{figures/vortex/v.png}
  \caption{Normalized pressure (left), u-velocity (right) and v-velocity
    (center) plots at $x=1.7m$ with time for 2D vortex advection with $1m/s$.}
  \label{fig:vortex_pu}
\end{figure}
In this test case, a vortex is generated in the inlet moving with a constant
velocity of $1m/s$ and allowed to go through the outlet. This case tests the
outlet for permeability for a velocity variation similar to vortex shedding.
It is important that this be preserved for most engineering flow simulations.
The domain size is kept same as in case of 2D pulse however the width is
doubled to accommodate the vortex. The vortex is generated by changing the
velocity at the inlet with time using
\begin{equation}
  (u, v) = \left(1.0 + \frac{\Gamma y}{r^2 + 0.2}, \frac{-\Gamma x}{r^2 +
  0.2}\right),
  \label{eq:vortex}
\end{equation}
where $\Gamma=0.1$ is the vortex strength, and $r = \sqrt{x^2+y^2}$ is the
distance from the center of the vortex. In order to calculate the distance of
the vortex we used
\begin{equation}
  x = u (1 - t),
  \label{eq:vortexx}
\end{equation}
where $u$ is the speed of the vortex and $1.0$ is the initial distance of the
vortex center from the beginning of the inlet. We test the vortex advection
with the methods and compare them with the results for a long domain. In
Figure~\ref{fig:vortex_pu}, we have plotted the pressure and velocity for the
different methods. In Table~\ref{table:vortex_l2} the $L_2$ errors of the
pressure and velocity are shown. It is evident from the plots that, the
do-nothing, modified do-nothing, and our new hybrid method match the results
of a long domain well. However, in case of mirroring technique a lot of back
pressure fluctuation is visible. The MOC shows a significant deviation from
the long domain and also does not preserve the velocity variation. In the
pressure plot, we can see that the mirror method shows a perfect match before
the vortex reaches the probe, thus it is suitable for outlets with very low
gradients. However, once the vortex reaches the probe, the results of the
mirror method are very poor. The $L_2$ errors clearly show that the do-nothing
methods and the hybrid schemes work well. In particular, the error in $p$ and
$v$ is high for the mirror and method of characteristic.
\begin{table}[h!]
\centering
\input{figures/vortex/vortex_l2.tex}
\caption{$L_2$ error in the $p^*, u^*$, and $v^*$ measured at the probe for
  the moving vortex problem.}
\label{table:vortex_l2}
\end{table}

As mentioned earlier, all our simulations are inviscid which suggests an
infinite Reynolds number. However, to investigate the effect of Reynolds
number on the outlets we performed the above simulation at $Re=100$ and
$10000$. In Table~\ref{table:vortex_re100_l2} and
\ref{table:vortex_re10000_l2}, we show the errors for $Re=100$ and $10000$
respectively. We can clearly see that the hybrid method and do-nothing
have low errors compared other methods.  We can also see that as the Reynolds
number reduces and the fluid becomes increasingly viscous that the errors in
the method of characteristics as well as the mirror method reduce.  This
clearly shows the importance of the new method.

\begin{table}[h!]
\centering
\input{figures/vortex_re100/vortex_re100_l2.tex}
\caption{$L_2$ error in the $p^*, u^*$, and $v^*$ measured at the probe for
  the moving vortex problem, with $Re=100$.}
\label{table:vortex_re100_l2}
\end{table}
\begin{table}[h!]
\centering
\input{figures/vortex_re10000/vortex_re10000_l2.tex}
\caption{$L_2$ error in the $p^*, u^*$, and $v^*$ measured at the probe for
  the moving vortex problem, with $Re=10000$.}
\label{table:vortex_re10000_l2}
\end{table}

\subsection{2D backward-facing step}
\label{sec:bfs}

We consider the 2D backward-facing step problem. Following the experimental
work of \citet{Armaly1983}, the step height is set as, $h=4.9mm$ with inlet
width $h_{i}=5.2mm$. We compare the velocity profile at different stations
with $x/h= 2.55, 3.57, 4.80, 7.14$ (where $x$ is the distance downstream from
the step). We compare our results with the experimental results in
\cite{Armaly1983}. The Reynolds number of the flow is chosen to be $389$ since
above this the flow is no longer two-dimensional. In the simulation, we set
$\rho=1.225kg/m^3$ and the viscosity is calculated using $Re=2 \bar{U} h / \nu
$, where $\bar{U}=2/3 U_{max}$ is the mean velocity. The inlet velocity is set
to $1 m/s$. The schematic of the simulation model is shown below

\begin{figure}[h!]
	\centering
  \includegraphics[width=\textwidth]{images/bfs.pdf}
	\caption{Sketch of domain used for backward-facing step simulations (all
    dimensions in mm).}
	\label{fig:bfs}
\end{figure}

At the walls, we satisfy the no-slip boundary condition. However, since the
inlet is set at a constant velocity, a no-slip wall introduces non-physical
pressure fluctuations. Thus a small part of the initial wall is set as a slip
wall. Similarly, near the outlet we allow slip at the wall in order to avoid
vortices at the start of the flow. In this test, we have shown results for our
proposed method and do-nothing only, since the characteristic method and
mirror methods failed to complete. In case of the mirror method, the vortices
reach the outlet and the simulation blows up. In case of characteristics, the
criteria for reference parameter is not known. In Figure~\ref{fig:bfs_u}, we
show the velocity profile for all the methods. It is evident from the plot
that all the methods (hybrid, do-nothing, and modified do-nothing) are able to
reproduce the results presented by \cite{Armaly1983}.

\begin{table}[h!]
\centering
\input{figures/backward_facing_step/backward_facing_step_rl.tex}
\caption{The reattachment length for $Re=389$ for different outlet implementations.}
\label{table:bfs_rl}
\end{table}

\begin{figure}[ht!]
	\centering
  \includegraphics[width=0.8\textwidth]{figures/backward_facing_step/u.png}
	\caption{Velocity at $t = 1.2 sec$ for $Re=389$ at different locations.}
	\label{fig:bfs_u}
\end{figure}

The reattachment length for the primary vortex is determined and presented in
Table~\ref{table:bfs_rl}. We can clearly see that the reattachment length is very
close to the experimental value $7.94$ from \cite{Armaly1983}. This testcase clearly
shows that the proposed method shows very less difference from the
experimental values compared to other methods. This also highlights the ability
of new proposed testcase to distinguish between truly non-reflecting outlet boundaries.

\subsection{Flow past circular cylinder}
\label{sec:cylinder}

\begin{figure}[h!]
	\centering
  \includegraphics[width=\textwidth]{images/fpc.pdf}
	\caption{Sketch of domain used for flow past circular cylinder simulations.}
	\label{fig:fpc}
\end{figure}

\begin{figure}[ht!]
	\centering
  \includegraphics[width=0.8\textwidth]{figures/flow_past_cylinder/p0.png}
	\caption{Normalized pressure at $t = 50 sec$ for $Re=200$.}
	\label{fig:pplots}
\end{figure}

\begin{figure}[ht!]
	\centering
  \includegraphics[width=0.8\textwidth]{figures/flow_past_cylinder/vmag0.png}
	\caption{Normalized velocity at $t = 50 sec$ for $Re=200$.}
	\label{fig:vplots}
\end{figure}

\begin{figure}[ht!]
	\centering
  \includegraphics[width=0.8\textwidth]{figures/flow_past_cylinder/p1.png}
	\caption{Normalized pressure at $t = 150 sec$ for $Re=200$.}
	\label{fig:pplots1}
\end{figure}

\begin{figure}[ht!]
	\centering
  \includegraphics[width=0.8\textwidth]{figures/flow_past_cylinder/vmag1.png}
	\caption{Normalized velocity at $t = 150 sec$ for $Re=200$.}
	\label{fig:vplots1}
\end{figure}
The flow past a circular cylinder is a well known benchmark to show the
capability of inlet/outlet boundaries. We investigate the problem for all the
methods described in this paper. We consider a smaller domain compared to
earlier research with fewer particles to show the effectiveness of the
proposed method~\cite{open_bc:tafuni:cmame:2018, MARRONE2013456}. A cylinder
of diameter $D (=2m)$ has been considered. The channel width is $15D$ to avoid
the effect of wall and the length is $15D$, which is aligned along the x-axis.
The cylinder is at $5D$ from the inlet interface as shown in Figure
\ref{fig:fpc}. Each inlet, outlet and wall has $6$ layers of particles which
are enough to get full kernel support. The inlet is given a constant
prescribed velocity, $u_\infty=1 m/s$. The walls function as a slip wall in
order to avoid effect of boundary layer from the walls. The fluid properties
such as kinematic viscosity of the flow is evaluated using $\nu = u_\infty
D/Re$, where $Re$ is the Reynolds number of the flow and density $\rho = 1000
kg/m^3$. We use a particle spacing $\Delta x=0.0667$ and $h/\Delta x=1.2$
which result in $201694$ fluid particles in the domain. This spacing results
in a cell Reynolds number of $Re_{cell}=u_\infty h/\nu$ of 8. This suggests
that this is a coarse simulation. In order to capture the curvature of the
cylinder, we place the particles in the solid such that the volume is
consistent. We first place particles on the circumference spaced $\Delta x$
from each other and then create points on a circle $\Delta x$ towards center
and perform the same procedure until we reach the center.

We simulate the model for $Re=200$ for all the methods. In the
Fig.~\ref{fig:pplots} and \ref{fig:vplots} we have plotted $p^*$ and $u^*$
respectively at $t=50s$ for all methods. Since the gradient near the outlet
boundary is close to zero, all the methods show similar variations. However
after vortex shedding starts, the gradient near the outlet is large. In
Fig.~\ref{fig:pplots1} and \ref{fig:vplots1} we show the pressure and velocity
distribution at $t=150s$ respectively, when the vortex shedding is well
established. In case of mirror method due to high gradient near the outlet,
spurious pressures arise and the particle positions diverge. It is evident
from the pressure plots in Fig.~\ref{fig:pplots1} that the MOC reflects the
pressure back into the domain when vortex shedding starts. In case of
do-nothing and modified do-nothing a significant increase in pressure of the
domain is visible. Pressure for both hybrid and characteristic method looks to
be distributed around zero which is essential for low numerical errors in
pressure calculations. In Fig.~\ref{fig:vplots1} of velocity distributions,
all the methods show a similar pattern and it is hard to comment on the
relative
merits of the methods.\\
\begin{figure}[h!]
	\centering
  \includegraphics[width=\textwidth]{figures/flow_past_cylinder/cl_comp.png}
  \caption{Plot for $c_d$ for all methods at Re=200}
	\label{fig:cdplots}
\end{figure}
In order to check the accuracy of the methods, we calculate the drag
$(F_d=F_x)$ and lift ($F_l=F_y$) forces on the cylinder for all the cases and
evaluate the coefficient of drag, $c_d=F_d / (0.5 \rho u_{\infty}^{2})$, and
lift $c_l=F_l / (0.5 \rho u_{\infty}^{2})$. A five point average is taken to
filter the noise. The force on the solid cylinder is determined by solving the
momentum equation given by
\begin{align}
	\label{eq:mom-newt_solid}
  \frac{F_{solid}}{m_{solid}} &= -\frac{1}{\rho} \nabla p +
  \nu \nabla^2 \ten{u}.
\end{align}
The above equation in the SPH form is given by
\begin{equation}
	\label{eq:tvf-momentum_solid}
	\begin{split}
    F_{solid} = \sum_j \left( V_i^2 +
		V_j^2 \right) & \left[ - \tilde{p}_{ij} \nabla W_{ij} +
      \tilde{\eta}_{ij} \frac{\ten{u}_{ij}}{(r_{ij}^2 + \eta
    h_{ij}^2)} \nabla W_{ij}\cdot \ten{r}_{ij} \right],
	\end{split}
\end{equation}
where all symbols have same meaning as given in section \ref{sec:sph} except
$u_{ij} = u_{g_i} - u_{j}$, where $u_{g_{i}}$ is the solid wall velocity
\cite{zhang_hu_adams17}. We also evaluate the Strouhal number $St=fD/u_\infty$ where
$f$ is the frequency of shedding. In Fig.~\ref{fig:cdplots}, we compare $c_l$
for all the methods over time. It can be easily seen that mirror method blows
up after a large back pressure. In case of do-nothing and modified do-nothing,
shedding starts earlier compared to hybrid and characteristic method. In
Table~\ref{table:clcd}, we compare $c_d$ and $c_l$ and $St$ for all the
methods and with results published by \cite{guerrero2009numerical,
  MARRONE2013456, open_bc:tafuni:cmame:2018}. We can see that in spite of
having non-physical pressure variations in characteristic methods the value of
$c_d$ and $c_l$ shows a close match. In case of both do-nothing and modified
do-nothing the values are close since the pressure increase of the domain is
insignificant in case of incompressible flows. In our proposed hybrid method,
the pressure and velocity plots looks similar to
\cite{open_bc:tafuni:cmame:2018, MARRONE2013456}, the $c_d$ and $c_l$ are in
acceptable range presented in literature.
\begin{table}[h!]
\centering
\input{figures/flow_past_cylinder/flow_past_cylinder_cd_cl_st.tex}
\caption{Comparison of $c_l$, $c_d$ and $St$ values for different method with
literature for $Re=200$.}
\label{table:clcd}
\end{table}
Furthermore, the proposed hybrid and modified do-nothing methods have been tested
for Reynolds number 20.
\begin{figure}[h!]
	\centering
  \includegraphics[width=\textwidth]{figures/flow_past_cylinder/re.png}
  \caption{Plot for $c_d$ and $c_l$ for hybrid and the modified do-nothing at
    Re=20 and 200.}
	\label{fig:cdclplots}
\end{figure}
\begin{table}[h!]
\centering
\input{figures/flow_past_cylinder/flow_past_cylinder_conv.tex}
\caption{Convergence of $c_d$ with the decrease in particle
  spacing at Re=20 for hybrid method}
\label{table:hybridconv}
\end{table}
In the Fig.~\ref{fig:cdclplots}, we show the $c_d$ and $c_l$ for hybrid and
modified do-nothing. It is evident from the figure that both modified
do-nothing and hybrid produces similar results. However hybrid is better than
the modified do-nothing as shown in other test cases. In order to check the
convergence of the results, we perform a convergence study for both the
proposed methods and tabulated the results in Table~\ref{table:hybridconv} at
$Re=20$. We observe that $c_d$ decreases with decrease in particle spacing and
converges to around 2.317. When the spacing is $0.05$ the cell Reynolds number
is 0.6 suggesting a sufficiently resolved simulation. It must be noted that
the results presented are in a smaller domain and with much fewer particles
than those used in earlier research which show that the proposed methods
replace need of a large domain for wind-tunnel type of simulations. The
results above also show that the flow past a circular cylinder does not reveal
important differences between the different boundary conditions and the
importance of our proposed test problems.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% fill-column: 78
%%% End:
