\documentclass[preprint,12pt]{elsarticle}

%\usepackage[hidelinks]{hyperref}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{booktabs}

\usepackage[utf8]{inputenc}

% For the TODOs
\usepackage{xcolor}
\usepackage{xargs}
\usepackage[colorinlistoftodos,textsize=footnotesize]{todonotes}
\newcommand{\todoin}{\todo[inline]}
% from here: https://tex.stackexchange.com/questions/9796/how-to-add-todo-notes
\newcommandx{\unsure}[2][1=]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,#1]{#2}}
\newcommandx{\change}[2][1=]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue,#1]{#2}}
\newcommandx{\info}[2][1=]{\todo[linecolor=OliveGreen,backgroundcolor=OliveGreen!25,bordercolor=OliveGreen,#1]{#2}}

%Boldtype for greek symbols
\newcommand{\teng}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\ten}[1]{\ensuremath{\mathbf{#1}}}

\usepackage{lineno}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{calc,quotes,angles}
\usetikzlibrary{shapes,snakes}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{calc,quotes,angles}
\tikzset{%
  >={Latex[width=2mm,length=2mm]},
  % Specifications for style of nodes:
            base/.style = {rectangle, rounded corners, draw=black,
                           minimum width=4cm, minimum height=1cm,
                           text centered, font=\sffamily},
  activityStarts/.style = {base, fill=blue!30},
       startstop/.style = {base, fill=red!30},
    activityRuns/.style = {base, fill=green!30},
         process/.style = {base, minimum width=2.5cm, fill=orange!15,
                           font=\ttfamily},
}
\journal{}

\begin{document}

\begin{frontmatter}

  \title{An improved non-reflecting outlet boundary condition for
    weakly-compressible SPH}
  \author[IITB]{Pawan Negi\corref{cor1}}
  \ead{pawan.n@aero.iitb.ac.in }
  \author[IITB]{Prabhu Ramachandran}
  \ead{prabhu@aero.iitb.ac.in}
  \author[IITB]{Asmelash Haftu}
  \ead{asmelash.a@aero.iitb.ac.in}
\address[IITB]{Department of Aerospace Engineering, Indian Institute of
  Technology Bombay, Powai, Mumbai 400076}

\cortext[cor1]{Corresponding author}

\begin{abstract}
  Implementation of an outlet boundary condition is challenging in the context
  of the weakly-compressible Smoothed Particle Hydrodynamics method. We
  perform a systematic numerical study of several of the available techniques
  for the outlet boundary condition. We propose a new hybrid approach that
  combines a characteristics-based method with a simpler frozen-particle
  (do-nothing) technique to accurately satisfy the outlet boundary condition
  in the context of wind-tunnel-like simulations. In addition, we suggest some
  improvements to the do-nothing approach. We introduce a new suite of test
  problems that make it possible to compare these techniques carefully. We
  then simulate the flow past a backward-facing step and circular cylinder.
  The proposed method allows us to obtain accurate results with an order of
  magnitude less particles than those presented in recent research. We provide
  a completely open source implementation and a reproducible manuscript.
  \end{abstract}

\begin{keyword}
%% keywords here, in the form: keyword \sep keyword
{SPH}, {inlet}, {outlet}, {boundary conditions}, {Entropically Damped
  Artificial Viscosity}

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)

\end{keyword}

\end{frontmatter}

% \linenumbers

\section{Introduction}
\label{sec:intro}

The Smoothed Particle Hydrodynamics (SPH) method was independently introduced
by \citet{monaghan-gingold-stars-mnras-77}, and \citet{lucy77} for simulation
of astrophysical problems. Ever since, many SPH schemes have been introduced
to solve a variety of fluid flow and elastic-dynamics problems (see
\cite{monaghan-review:2005} for a review). \citet{sph:fsf:monaghan-jcp94}
introduced the weakly-compressible SPH (WCSPH) to deal with incompressible
fluids like water. An equation of state is introduced to relate the pressure
to the density. There are two common problems with the WCSPH schemes. The
first is the presence of particle disorder which reduces the accuracy of the
scheme and the second is the presence of large pressure oscillations due to
the stiff equation of state. Particle disorder can be ameliorated by the use
of the Transport Velocity Formulation (TVF) \cite{Adami2013,zhang_hu_adams17}
or by using particle
shifting~\cite{diff_smoothing_sph:lind:jcp:2009,fickian_smoothing_sph:skillen:cmame:2013}.
The pressure oscillations can be reduced by using a density
smoothing~\cite{wcsph-state-of-the-art-2010}, or by use of the $\delta$-SPH
formulation~\cite{antuono-deltasph:cpc:2010}. In the present work, we have
used the Entropically Damped Artificial Compressibility SPH (EDAC SPH)
\cite{edac-sph:cf:2019} method that introduces a pressure evolution equation
that damps any pressure oscillations. In addition to the WCSPH schemes
discussed above, there are also a family of truly Incompressible SPH
schemes~\cite{sph:psph:cummins-rudman:jcp:1999,isph:shao:lo:awr:2003,isph:hu-adams:jcp:2007}
(ISPH). These schemes solve for a pressure-Poisson equation to find a suitable
pressure distribution. These schemes require that a large, sparse system of
linear equations be solved in order to compute the pressure.

Despite the many developments in the SPH method, there are some challenges in
implementing accurate non-reflecting boundary conditions (NRBC) with the
weakly-compressible formulations. One significant objective in implementing
inlet and outlet boundary conditions is to let the pressure and velocity
fluctuations pass out of the domain without affecting the internal particles.
\citet{Lastiwka2009:nonrefbc} addressed this by extrapolating properties from
within the fluid. To obtain first order consistency near the inlet and outlet,
the reproducing kernel particle method given by \citet{Liu1995:rkpm} is used.
Within the fluid, the corrected gradient given by \citet{bonet_lok:cmame:1999}
is used for accurate results. Any perturbations are passed out of the domain
using characteristic variables and carefully chosen boundary conditions based
on these characteristic variables.

\citet{FEDERICO201235} proposed freezing the properties of the fluid particles
in the outlet. The outlet particles are advected with the frozen velocity.
\citet{MARRONE2013456} utilized the approach suggested by
\citet{FEDERICO201235} and \citet{Lastiwka2009:nonrefbc} to simulate flows
around bluff-bodies for a wide range of Reynolds numbers.
\citet{MOLTENI201378} proposes using a sponge layer in order to absorb waves
coming from the domain in order to implement a non-reflective boundary and
tested it on water waves inside a tank. In a method suggested by
\citet{khorasanizade-open-boundary-sph:ijnmf-2015}, the fluid is divided into
multiple sections perpendicular to the flow and the values from these zones
are used to impose natural boundary conditions (zero-gradients of properties)
to the inlet/outlet.

Recently, \citet{ALVARADORODRIGUEZ2017177} modified the NRBCs proposed by
\cite{JIN1993239} for SPH. \citet{open_bc:tafuni:cmame:2018} proposes the use
of ghost (or buffer) particles for inlet/outlet particles and use the higher
order interpolation scheme of \citet{LIU200619} to extrapolate the property
using a Taylor series expansion. Their approach allows them to treat the
outlet and inlet buffer particles in the same way. \citet{pwang_2019} use the
characteristic wave propagation velocity and perform Lagrange interpolation in the time
domain to correctly implement NRBCs to simulate an under water blast in a
small domain.

In the context of ISPH schemes, \citet{HOSSEINI20117473} suggested a
rotational pressure correction scheme in order to extrapolate pressure to the
inlet or outlet and thereby impose natural boundary conditions. At the outlet,
the last layer of fluid is copied up to a sufficient distance to ensure kernel
support for the fluid particles. \citet{PAHAR2017464} satisfy a
divergence-free condition for the inlet and outlet by solving a
pressure-Poisson equation along with the fluid particles.
\citet{MONTELEONE20179} investigated a novel approach in which only pressure
boundary conditions were prescribed and velocity profiles are allowed to
change according to it.

In the present work we focus on weakly-compressible schemes. It is clear that
the method proposed by \citet{Lastiwka2009:nonrefbc} is ideal when one wishes
to extrapolate properties from the fluid. This is most useful for inlets where
one needs to extrapolate the pressure from the fluid into the inlet and
prescribe the inlet velocity alone. However, for outlets, it is not clear
which one of these methods is ideal for bluff body simulations. We find that
there are a few important considerations that are not fully discussed in any
of the earlier studies. Specifically, many realistic flows involving an outlet
will have large vortices leaving the domain. These vortices involve both a
pressure and velocity gradient. It is important that any outlet boundary
condition not destroy these structures as doing so would affect the vortices
upstream. These are typically handled by simply increasing the domain but this
may not be needed if the outlet is carefully implemented. WCSPH schemes
constantly generate pressure waves. These may be severe if the bodies are
oscillating and this would introduce additional pressure waves which should be
propagated out of the domain without vitiating any physical gradients like
those due to vortices.

Efficiently testing an SPH outlet implementation in the context of the above
issues is critical. Doing so using a flow past cylinder benchmark is
inefficient. We propose a suite of simple and efficient test problems that
allow us to systematically investigate the boundary conditions. The benchmarks
are the simple one-dimensional benchmark proposed by
\citet{Lastiwka2009:nonrefbc}, a two-dimensional wave, a free-vortex advecting
with a mean flow, and a ramp inlet.

We implement the following boundary conditions and test them with the above
benchmark problems and bring out the relative merits of each. The methods we
implement are,
\begin{itemize}
\item a simple do-nothing boundary
  condition~\cite{khorasanizade-open-boundary-sph:ijnmf-2015,ALVARADORODRIGUEZ2017177}
  where the particle properties are frozen. We propose an improvement to this
  method .
\item extrapolating the fluid properties to the outlet as proposed by
  \citet{open_bc:tafuni:cmame:2018} using a higher order interpolation.
\item propagate the properties of the fluid into the outlet using the
  method of characteristic (MOC)~\citet{Lastiwka2009:nonrefbc}.
\item a new hybrid approach that combines the do-nothing and MOC methods.
\end{itemize}

Based on our careful study, we see that all the existing methods have some
difficulties. The hybrid method uses the best features of the available
methods and performs much better with our test problems. The proposed
modification to the traditional do-nothing also produces fairly good results
and is very easy to implement. We finally apply these to the flow past a
backward-facing step and a cylinder for Reynolds numbers in the range 20-200.
We present the results in the entire computational domain showing the
effectiveness of our implementation. The new boundary conditions allow us to
obtain reasonable results with an order of magnitude fewer particles than
previous results.

We use the open source PySPH~\cite{PR:pysph:scipy16,pysph} framework for our
simulations. Furthermore, in the interest of reproducible research, every
figure presented in the results section of this manuscript is
automated~\cite{pr:automan:2018} and the source code is made available at
\url{https://gitlab.com/pypr/inlet_outlet}. In the next section, we describe
the SPH scheme we employ in some detail. Section \ref{sec:bc}, discusses the
different techniques used to implement the outlet boundary conditions.
Section~\ref{sec:results} introduces the new test problems and compares the
different boundary condition implementations.


\section{The SPH method}
\label{sec:sph}

In the present work, the EDAC (Entropically Damped Artificially Compressible)
SPH scheme~\cite{edac-sph:cf:2019} is used to simulate incompressible fluid
flow. The EDAC scheme uses a pressure evolution equation that is similar to
the continuity equation but also contains a pressure damping term which reduces
oscillations. The basic equations are the momentum equation,
\begin{align}
	\label{eq:mom-newt}
	\frac{d \ten{u}}{d t} &= -\frac{1}{\rho} \nabla p +
	\nu \nabla^2 \ten{u},
\end{align}
where $\ten{u}$ is the velocity of the fluid, $p$ is the pressure, and $\nu$
is the kinematic viscosity of the fluid. The EDAC pressure equation is given
as,
\begin{equation}
 	\label{eq:p-evolve}
	\frac {d p}{d t} = - \rho c _ {s}^{2} \operatorname {div} (
	\mathbf {u}) + \nu_{edac} \nabla ^{2} p,
	%\label{eq:edacPress}
\end{equation}
where $c_s$ is the speed of sound, and the second term in the right hand side
is the damping term and the viscosity used there is chosen as,
\begin{equation}
  \label{eq:edac-nu}
  \nu_{edac} = \frac{\alpha h c_s}{8}.
\end{equation}
$\alpha$ is chosen as 0.5, $h$ is the SPH kernel smoothing length which is
discussed further below and $c_s$ is chosen such that $c_s = 10\ u_{\max}$
where $u_{\max}$ is an estimated maximum speed in the flow.

The EDAC SPH formulation~\cite{edac-sph:cf:2019} comes in two flavors. As we
are primarily solving problems without a free surface in this work, we use the
EDAC TVF formulation which employs the Transport Velocity Formulation of
\cite{Adami2013} along with the EDAC equation for evolving pressure,
equation~\eqref{eq:p-evolve}. This formulation ensures that the particle
distribution is uniform through the use of a background pressure.

Particle volume for a particle $i$ is evaluated using $m_i/\rho_i$ where
$\rho_i$ is evaluated using the summation density,
\begin{equation}
	\label{eq:summation-density}
	\rho_i = \sum_j m_j W_{ij},
\end{equation}
where $W_{ij} = W(|\ten{r_i} - \ten{r_j}|, h)$ is the kernel function chosen
for the SPH discretization and $h$ is the kernel radius parameter. The
summation is over all the neighbors of particle $i$. In this paper, the
quintic spline kernel is used, which is given by,
\begin{equation}
	\label{eq:quintic-spline}
	W(q) = \left \{
	\begin{array}{ll}
		\alpha_2 \left[ {(3-q)}^5 - 6{(2-q)}^5 + 15{(1-q)}^5 \right],\
		& \textrm{for} \ 0\leq q \leq 1,\\
		\alpha_2 \left[ {(3-q)}^5 - 6{(2-q)}^5 \right],
		& \textrm{for} \ 1 < q \leq 2,\\
		\alpha_2 \ {(3-q)}^5 , & \textrm{for} \ 2 < q \leq 3,\\
		0, & \textrm{for} \ q>3,\\
	\end{array} \right.
\end{equation}
where $\alpha_2 = 7/(478\pi h^2)$ in two-dimensions, and $q=|\ten{r}|/h$.

The present work utilizes a number density based formulation as discussed in
\cite{edac-sph:cf:2019}. The resulting discretized momentum equation is as
follows:
\begin{equation}
	\label{eq:tvf-momentum}
	\begin{split}
		\frac{\tilde{d} \ten{u}_i}{d t} = \frac{1}{m_i} \sum_j \left( V_i^2 +
		V_j^2 \right) & \left[ - \tilde{p}_{ij} \nabla W_{ij} +
		\frac{1}{2}(\ten{A}_i + \ten{A}_j) \cdot \nabla W_{ij} \right . \\ &
		\left .  + \tilde{\eta}_{ij} \frac{\ten{u}_{ij}}{(r_{ij}^2 + \eta
			h_{ij}^2)} \nabla W_{ij}\cdot \ten{r}_{ij} \right] + \ten{g}_i,
	\end{split}
\end{equation}
where $\ten{A} = \rho \ten{u}(\ten{\tilde{u}} - \ten{u})$, $\ten{\tilde{u}}$
is the advection or transport velocity and $\frac{\tilde{d}}{dt}$ is the
material derivative associated with this transport velocity. $\ten{r}_{ij} =
\ten{r}_i - \ten{r}_j$, $\ten{u}_{ij} = \ten{u}_i - \ten{u}_j$, $h_{ij} = (h_i
+ h_j)/2$, $\eta=0.01$, $ V_i = \frac{1}{\sum_j W_{ij}} $, and
$\tilde{\eta}_{ij} = \frac{2 \eta_i \eta_j}{\eta_i + \eta_j}$, where $\eta_i =
\rho_i \nu_i$. An average pressure is subtracted to reduce errors in the
pressure gradient. The average pressure is found as,
\begin{equation}
	\label{eq:pavg}
	p_{\text{avg}, i} = \sum_{j=1}^{N_i} \frac{p_j}{N_i},
\end{equation}
where $N_i$ are the number of neighbors for the particle $i$ and includes both
fluid and boundary neighbors. This average pressure is used to define
$\tilde{p}_{ij}$ as,
\begin{equation}
	\label{eq:tvf-p-ij-basa}
	\tilde{p}_{ij} =
	\frac{\rho_j (p_i-p_{avg, i}) + \rho_i (p_j - p_{avg, i})}{\rho_i + \rho_j}.
\end{equation}

The EDAC pressure evolution equation (\ref{eq:p-evolve}) is discretized
using a similar approach to the momentum equation as,
\begin{align}
	\label{eq:p-edac}
	\frac{d p_i }{dt} &=  \sum_j \frac{m_j \rho_i}{\rho_j} c_s^2 \ \ten{u_{ij}}
	\cdot \nabla W_{ij} + \frac{(V_i^2 + V_j^2)}{m_i} \tilde{\eta}_{ij}
	\frac{p_{ij}}{(r_{ij}^2 + \eta h_{ij}^2)} \nabla W_{ij}\cdot \ten{r}_{ij},
\end{align}
where $p_{ij} = p_i - p_j$.

The particles move using the transport velocity as,
\begin{equation}
	\label{eq:advection}
	\frac{d \ten{r}_i}{dt} = \ten{\tilde{u}}_i.
\end{equation}
The transport velocity is obtained from the momentum velocity $\ten{u}$ at
each time step using,
\begin{equation}
	\label{eq:transport-vel}
	\ten{\tilde{u}}_i(t + \delta t) = \ten{u}_i(t) +
	\delta t \left(
	\frac{\tilde{d} \ten{u}_i}{dt}
	- \frac{p_b}{m_i} \sum_j \left( V_i^2 + V_j^2 \right)
	\nabla W_{ij}
	\right),
\end{equation}
where $p_b$ is the background pressure. We choose the timestep and other
parameters as discussed in \cite{edac-sph:cf:2019}. The solid wall boundary
conditions are implemented as discussed in \cite{Adami2012,edac-sph:cf:2019}
and use a layer of ghost particles inside the solid. The pressure and velocity
of the fluid is suitably projected on the solid. While we have employed the
EDAC SPH scheme in our computations, we could have employed any WCSPH-based
scheme for the purposes of this study.

\section{Boundary conditions }
\label{sec:bc}

In this work we are interested in simulating incompressible flow and in all of
our test problems we have a prescribed velocity at the inlet. In order for a
fluid particle with support radius $h$ to have full support, one requires
outlet/inlet particles. Fig.~\ref{fig:io_schematic}, shows a schematic for the
particles at the inlet, outlet, and fluid. The particle properties at the
inlet and outlet are evaluated using those of the fluid. As described in the
previous section, the EDAC SPH scheme employs a pressure evolution equation
that is not directly related to the fluid density. We therefore extrapolate
pressure from the fluid to the inlet using the mirroring technique as
described in Section \ref{subsec:mirror}. At the outlet, one needs to
determine values of both the velocity and pressure. In this paper, we first
evaluate the different existing approaches for implementing outlets and
propose improvements in order to simulate NRBCs. In the following subsections,
we describe the methods that we implement.

\begin{figure}[h!]
  \centering
  \resizebox{\linewidth}{!}{
    \input{io.tex}
  }
  \caption{Sketch of the inlet, fluid, and outlet particle arrangement.  The
    support for one outlet particle is also shown.}
  \label{fig:io_schematic}
\end{figure}
\subsection{Do-nothing}
\label{subsec:donothing}

\citet{JIN1993239} advect the outgoing waves that pass through the outlet
without reflecting them back into the domain for a mesh-based method using the
MOC. The equation which can be used to propagate a wave through the outlet is
given by
\begin{equation}
  \frac{\partial \ten{u}}{\partial t}+u \frac{\partial
  \ten{u}}{\partial x}- \nu \frac{\partial^{2} \ten{u}}{\partial
  y^{2}}=0,
  \label{eq:nrbcnew}
\end{equation}
where $\ten{u}$ is the velocity vector, $u$ is velocity component in $x$
direction and $\nu$ is the kinematic viscosity. In this paper, the diffusion
term has been dropped since the time for which outlet particles interact with
the fluid particles is not long enough for diffusion. Thus the
equation~\eqref{eq:nrbcnew} reduces to
\begin{equation}
  \frac{\partial \ten{u}}{\partial t}+u \frac{\partial
  \ten{u}}{\partial x}=0.
  \label{eq:nrbc2}
\end{equation}
\citet{ALVARADORODRIGUEZ2017177} proposes an SPH discretization of the
equation~\eqref{eq:nrbc2}, where the first term on the right-hand-side is
considered as a material derivative and the velocity is integrated by taking
the second term as acceleration. However, the equation~\eqref{eq:nrbc2}
physically means one must advect the particles in the normal direction to the
outlet while freezing all other properties like velocity and pressure. This is
similar to the method proposed by \citet{FEDERICO201235}. In SPH form, at the
outlet we can use
\begin{equation}
  x^{n}_{o} = x^{n-1}_{o} + u^{n-1}_{o}\Delta t,
  \label{}
\end{equation}
\begin{equation}
  u^{n}_o = u^{n-1}_{o}
  \label{}
\end{equation}
and
\begin{equation}
  p^{n}_{o} = p^{n-1}_{o},
  \label{}
\end{equation}
where $*^{n}_{o}$ denotes the outlet properties at time $n$ and $x$, $u$ and
$p$ are the position, $x$-component of the velocity and pressure respectively.


\subsubsection{Modified Do-nothing}
\label{subsubsec:mod_don}

We propose a subtle modification to the standard do-nothing method described
in section \ref{subsec:donothing}. Unlike the standard do-nothing where the
outlet moves with a velocity with which it left the fluid domain, we propose
to extrapolate the velocity of the fluid to the advection velocity of the
outlet particles.  Thus the advection is given by
\begin{equation}
	x^{n}_{o} = x^{n-1}_{o} + u^{n}_{ex}\Delta t,
  \label{}
\end{equation}
where $u^n_{ex}$ is the Shepard extrapolated fluid velocity at timestep $n$
given as
\begin{equation}
  \label{eq:shepard0}
  u_{ex} = \frac{\sum_j u_j W_{ij}}{\sum_j W_{ij}}.
\end{equation}
It must be noted that the advection velocities are only used to advect the
particles and the actual velocity of the outlet particles remain the ones
frozen when the fluid particle is converted to the outlet particle.

\subsection{Mirroring}
\label{subsec:mirror}

\citet{open_bc:tafuni:cmame:2018} employ a novel approach where the properties
at the inlet/outlet are extrapolated using a Taylor series expansion about a
mirrored particle at the fluid region. In the Fig.~\ref{fig:mirror_schematic},
we show the mirrored particles as circles with a dashed blue outline. The
mirror particles are generated by reflecting inlet/outlet particles about the
interface. Due to lack of kernel support at the interface, a higher order
approximation given by \citet{LIU200619} is used to determine the property
value at the mirrored particle.

\begin{figure}[h!]
  \centering
  \resizebox{\linewidth}{!}{
    \input{mirror.tex}
  }
  \caption{Inlet outlet particle arrangement. The dashed blue circles
    represent the reflected particles of the inlet and outlet about the
    interface.}
  \label{fig:mirror_schematic}
\end{figure}
In multiple dimensions, the first order Taylor series expansion for any
property $f$ of a particle about position $\mathbf{x}_k$ is
\begin{equation}
  f(\mathbf{x}) = f_{k} +f_{k,\beta}
  \left(\mathbf{x}-\mathbf{x}_{k}\right).
  \label{eq:liu1_taylor}
\end{equation}
Here $f_k = f(\ten{x}_k)$ and $f_{k,\beta}$ denotes the derivatives of the
function and $\beta \in {x, y, z}$. Taking the inner product of the function
with the SPH kernel $W_k(\ten{x}) = W(\ten{x} - \ten{x}_k)$ and it's
derivative $W_{k,\beta}(\ten{x}) = W_{\beta}(\ten{x} - \ten{x}_k)$, we obtain
\begin{equation}
  \int f(\mathbf{x}) W_{k} (\mathbf{x}) \mathrm{d} \mathbf{x} = f_{k} \int
  W_{k}(\mathbf{x}) \mathrm{d} \mathbf{x} + f_{k,\beta} \int
  \left(\mathbf{x}-\mathbf{x}_{k}\right) W_{k} (\mathbf{x}) \mathrm{d}
  \mathbf{x}
  \label{eq:liu1}
\end{equation}
and
\begin{equation}
  \int f(\mathbf{x}) W_{k,\beta} (\mathbf{x}) \mathrm{d} \mathbf{x} = f_{k}
  \int W_{k,\beta} (\mathbf{x}) \mathrm{d} \mathbf{x} + f_{k,\beta} \int
  \left(\mathbf{x} - \mathbf{x}_{k} \right) W_{k,\beta} (\mathbf{x})
  \mathrm{d} \mathbf{x}.
  \label{eq:liu2}
\end{equation}
Equations \eqref{eq:liu1} and \eqref{eq:liu2} can be written in matrix form
using SPH approximation as
\begin{equation}
  \left[\begin{array} {cccc} {W_{kl} V_{l}} & {x_{lk}  W_{kl} V_{l}} &
  {y_{lk} W_{kl} V_{l}} & {z_{lk} W_{kl} V_{l}} \\ {W_{kl,x} V_{l} } &
  {x_{lk}W_{kl,x} V_{l}} & {y_{lk}W_{kl,x} V_{l}} & {z_{lk}W_{kl,x} V_{l}}
  \\ { W_{kl,y} V_{l}} & {x_{lk}W_{kl,y} V_{l}} & {y_{lk}W_{kl,y}V_{l}} &
  {z_{lk}W_{kl,y}V_{l} } \\ {W_{kl,z}V_{l}} & {x_{lk}W_{kl,z}V_{l}} &
  {y_{lk}W_{kl,z}V_{l} } & {z_{lk}W_{kl,z}V_{l}} \end{array} \right] \left[
  \begin{array} {c} {f_{k}} \\ {f_{k,x}} \\ {f_{k,y}} \\ {f_{k,z}}
\end{array} \right] = \left[\begin{array} {c} {f_{l}W_{kl}V_{l} } \\
  {f_{l}W_{kl,x}V_{l}} \\ {f_{l}W_{kl,y}V_{l} } \\ {f_{l}W_{kl,z}V_{l} }
\end{array} \right],
  \label{eq:matform}
\end{equation}
where $W_{kl}$ and $W_{kl,\beta}$ : $\beta \in \{x,y,x\}$ are the kernel and
it's derivative respectively, $k$ denotes the destination index, $l$ denotes
the source particle index and $f$ is the property of interest, $V_k$ is the
volume of the $k$'th particle which in the present case is $m/\rho$, and
$x_{kl} = x_k - x_l$. For brevity repeated indices $l$, are summed over. Note
that the index $k$ indicates the target particle which is fixed and not summed
over. The above linear system is solved for each mirrored destination, which
gives the property and it's derivative. After evaluating $f$ and
$f_{k,\beta}$, the values at inlet/outlet are evaluated using the Taylor
series expansion given by
\begin{equation}
  f_{o} = f_{k} + \left(\mathbf{r}_{o} - \mathbf{r}_{k} \right) \cdot
  {\nabla} f_{k},
  \label{eq:taylor}
\end{equation}
about the corresponding ghost particle position $x_k$.
\citet{open_bc:tafuni:cmame:2018} extrapolate all the relevant properties
using equation \eqref{eq:taylor}. In this paper, we have modified the equation
for extrapolation to
\begin{equation}
  f_{o} = f_{k} - \left(\mathbf{r}_{o} - \mathbf{r}_{k} \right) \cdot
  {\nabla} f_{k}.
  \label{eq:taylor2}
\end{equation}
in order to get zero gradient at the outlet interface. When we use the
original form as written in \cite{open_bc:tafuni:cmame:2018}, the test cases
blow up.

\subsection{Method of characteristics}
\label{subsec:charac}

This method has been proposed by \citet{Lastiwka2009:nonrefbc}. The basic idea
is to resolve the perturbations from the mean flow in terms of the
characteristics and then use the characteristic variables to propagate the
appropriate values to the outlet or inlet. The scheme is itself based on the
work of \citet{giles:nrbc:aiaa:1990} who proposes general NRBCs for the Euler
equations.

The properties of fluid are rewritten in terms of the characteristic variables
perpendicular to the outlet. In this process the appropriate boundary
conditions may be applied. The following form of the characteristic variables
is used,
\begin{align}
  \nonumber
  J_1 &= -c_s^2 (\rho - \rho_{ref}) + (p - p_{ref}) \\ \label{eq:characteristic-vars}
  J_2 & = \rho c_s(u - u_{ref})  + (p - p_{ref}) \\   \nonumber
  J_3 & = -\rho c_s(u - u_{ref})  + (p - p_{ref}),
\end{align}
where the $u_{ref}, p_{ref}, \rho_{ref}$ denote the reference quantities in
the domain. We note that $J_1, J_2, J_3$ correspond to the quantities $c_1,
c_3, c_4$ in the work of \citet{giles:nrbc:aiaa:1990}. The outflow boundary
conditions basically require that $J_1$ and $J_2$ be determined from the
interior and that $J_3$ be set to zero. The perturbations in the plane of the
outlet pass through without any change, so the transverse components of the
velocity are not changed by this scheme.


\begin{figure}[h!]
  \centering
  \resizebox{0.5\linewidth}{!}{
    \input{moc.tex}
  }
  \caption{The outlet particles having fluid particles in their support radius
    are shown in red and without fluid particles in their support are in blue.}
  \label{fig:io_moc}
\end{figure}

In our implementation we use a simple Shepard interpolation given by
\begin{equation}
  \label{eq:shepard}
  f_{i} = \frac{\sum_{j}^{N} f_{j} W_{ij} }{\sum_{j}^{N} W_{ij}},
\end{equation}
where the $i^{th}$ outlet particle is outside the fluid domain as shown in
Fig.~\ref{fig:io_schematic} and $f$ is either $J_1$ or $J_2$. We use
equation~\eqref{eq:shepard} to interpolate $J_1, J_2$ from the fluid to the
outlet. Note that only some of the outlet particles are in the influence of
the fluid. Fig.~\ref{fig:io_moc} shows a sketch of the outlet and fluid. As can be seen, the
red particles are under the influence of the fluid but the blue particles are
not. For the blue particles in the outlet which are outside the influence of
the fluid particles i.e.\ $N=0$, we find the $J_1$ and $J_2$ using the average
of the values of red particles but at the previous timestep,
\begin{equation}
  \label{eq:average_old}
  f_{i}^{n} = \frac{\sum_{j}^{M} f_{j}^{n-1}}{M},
\end{equation}
where $M$ are the number of red particles which are in the support of the
$i$\textsuperscript{th} blue particle. Given $J_1, J_2, J_3$ we can easily
solve for the actual variables $u, p, \rho$ using
equation~\eqref{eq:characteristic-vars}.


\subsection{A new hybrid method}
\label{subsec:hybrid}
In this section, we describe a new method to implement outlet boundaries. At
the outlet, essentially two kinds of fluctuations are encountered namely
spatial variations which do not change rapidly in time and variations due to
acoustic waves which travel with the speed of sound. The weakly compressible
SPH schemes generate perturbations that travel with the prescribed speed of
sound unlike with ISPH schemes which solve for a pressure-Poisson equation. In
the case of a do-nothing type of outlet boundary as described earlier, the
particle properties are frozen. As a result, when the acoustic wave arrives at
the outlet, its velocity suddenly drops to the particle velocity in the
outlet. This causes an increase in the pressure for particles that are near
the outlet. In our proposed method, we devise a method to separate the fluid
flow properties into acoustic and base flow properties.

A time averaged property of the flow is given by
\begin{equation}
  f_{avg} = \frac{\sum_{n=1}^{N} f_n}{N},
  \label{eq:timeevg}
\end{equation}
where $f$ is the fluid property, $N$ is the number of time steps used in the
averaging. The value of $N$ can be estimated by determining the number of time
steps the acoustic wave takes to move from one particle to another given by
\begin{equation}
	N = \frac{\Delta x}{\Delta t (u+c_s)}.
	\label{eq:N}
\end{equation}
In all our cases $N \approx 4$, thus in order to have a sufficient time
average we take $N = 6$ for all our test cases. Further, in order to detect
the acoustic wave, the acoustic intensity is used as a parameter. The time
averaged properties are not changed whenever the acoustic intensity of the
flow is greater than the prescribed value. The acoustic intensity is given by
$p^2/(2\rho c_s)$ \cite{kinsler1999fundamentals}. The prescribed value of
acoustic intensity can be determined using the inlet velocity, $u_i$ and is given by
\begin{equation}
  I = \frac{(\frac{1}{2} \rho u_i^2)^2}{2 \rho c_s}.
  \label{eq:acouinten}
\end{equation}
The difference between the particle property and its time-average gives us the
acoustic component. The time-averaged part is advected out of the domain using
the do-nothing method. Since the acoustic wave travels with the speed of sound
it should be propagated out with the same. We use the method of
characteristics described in the previous section to propagate these acoustic
perturbations into the outlet, where the reference values are the
time-averages. In our implementation, we keep $\rho_{ref}$ fixed.

When a particle moves from the fluid domain into the outlet, it retains its
time average values. The acoustic properties are added to this using Shepard
interpolation to the outlet zone as
\begin{equation}
	f_o = f_{ac} + f_{avg},
  \label{eq:acou}
\end{equation}
where $f_{ac}$ is determined using the extrapolated $J_2$ as explained in
section \ref{subsec:charac}. Since the do-nothing condition is used at the
outlet for the time-averaged values, the proposed method cannot simulate
incoming flow near the outlet however it is suitable for wind tunnel type of
flow where the flow always exits the outlet from one side. The particles in
the outlet layer are advected using the velocity evaluated with equation
\eqref{eq:acou} (assuming the outlet is perpendicular to the x-axis). The
particles are not moved in the transverse direction. We note that the Shepard
interpolation of the properties from the fluid will not always carry to all
the particles in the outlet. These particles are advected with the average of
the existing outlet advection velocity.

For all the inlet/outlet methods described here, inlet particles are added to
the fluid particles whenever they cross the inlet-fluid interface. Similarly,
at the outlet, fluid particles are removed and added to outlet whenever they
cross the fluid-outlet interface. The particles are deleted once they leave the
outlet region.


\input{results}

\section{Conclusions}
\label{sec:conclusions}

In this paper we review the established techniques for implementing outlet
boundaries in the context of weakly-compressible SPH schemes. We classify them
into three broad categories. In order to systematically examine these, we
construct four simple test problems. These tests clearly show the deficiencies
of the existing approaches.
\begin{itemize}
\item{Do-nothing method is only suitable for problems where high intensity
    acoustic pressure waves are absent.}
\item {The mirror method works best for flows where the gradients are very low
    near the outlet.}
\item {The MOC show excellent results where reference properties are known a
    priori but are not very effective when there are gradients in the flow at
    the exit.}
\end{itemize}
Based on this, we propose a new generalized scheme which combines the
do-nothing and characteristic based outlet into a new hybrid technique. The
proposed technique works well with both high intensity acoustic waves and high
gradient flow near the outlet. Unlike the MOC, it calculates reference flow
variables by time averaging. We also propose a much simpler and slightly
modified do-nothing boundary condition that produces good results. We then
demonstrate these with simulations of the flow past a circular cylinder at two
different Reynolds numbers and also for the flow past a backward-facing step.
We are able to obtain very good results with much fewer particles than
reported earlier. Finally, our implementation is open source and our
manuscript is fully reproducible.

\section*{Acknowledgements}

The authors are grateful to Prof.~Krishnendu Haldar of the Department of
Aerospace Engineering, IIT Bombay for providing us with his
workstation to expedite our simulations.

\section*{References}
\bibliographystyle{model6-num-names}
\bibliography{references}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% fill-column: 78
%%% End:
